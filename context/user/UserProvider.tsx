import { useState } from 'react';

import Context from './UserContext';

const UserProvider: React.FC = ({ children }) => {
  const [address, setAddress] = useState<string>('');
  const [balance, setBalance] = useState<number>(0);

  return (
    <Context.Provider
      value={{
        address,
        setAddress,
        balance,
        setBalance,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export default UserProvider;
