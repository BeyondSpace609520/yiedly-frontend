import React from 'react';

interface UserContextInterface {
  address: string;
  setAddress: (address: string) => void;
  balance: number;
  setBalance: (balance: number) => void;
}

const UserContext = React.createContext<UserContextInterface | null>(null);

export default UserContext;
