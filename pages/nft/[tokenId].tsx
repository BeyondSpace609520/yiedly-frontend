import Head from 'next/head';
import { Detail } from '../../components/detail';
import { Layout } from '../../components/layout';
export default function DetailPage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - NFT</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
      </Head>
      <Layout>
        <Detail />
      </Layout>
    </>
  );
}
