import Head from 'next/head';
import Profile from '../../components/profile';
import { Layout } from '../../components/layout';
export default function ProfilePage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - Profile</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
      </Head>
      <Layout>
        <Profile />
      </Layout>
    </>
  );
}
