import Head from 'next/head';
import { Setting } from '../components/setting';
import { Layout } from '../components/layout';
export default function CreatePage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - Create</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
        <script
          src="https://unpkg.com/@themesberg/flowbite@latest/dist/flowbite.bundle.js"
          async
        ></script>
      </Head>
      <Layout>
        <Setting />
      </Layout>
    </>
  );
}
