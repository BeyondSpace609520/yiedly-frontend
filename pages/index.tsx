import Head from 'next/head';
import { Banner } from '../components/banner';
import { Category } from '../components/category';
import { Feature } from '../components/feature';
import { Layout } from '../components/layout';
import { Space } from '../components/space';
import { Trending } from '../components/trending';
export default function HomePage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - Home</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
      </Head>
      <Layout>
        <Banner />
        <Trending />
        <Category />
        <Space />
        <Feature />
      </Layout>
    </>
  );
}
