import '../styles/globals.scss';
import type { AppProps } from 'next/app';
import { ToastContainer } from 'react-toastify';
import { ThemeProvider } from '../theme/ThemeProvider';
import UserProvider from '../context/user/UserProvider';
import 'react-toastify/dist/ReactToastify.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider>
      <UserProvider>
        <Component {...pageProps} />
        <ToastContainer autoClose={5000} hideProgressBar />
      </UserProvider>
    </ThemeProvider>
  );
}
export default MyApp;
