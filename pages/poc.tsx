import Head from 'next/head';

import { Layout } from '../components/layout';
import AuctionCard from '../components/ui/auctionCard';

const cardData = {
  appId: '44256358',
  currencyType: 'ALGO',
  title: 'Magic Sport',
  description: 'Fotball beats all sports',
  image: '/img/nft/1.png',
  price: '1',
};

export default function PocPage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - POC</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
      </Head>
      <Layout>
        <AuctionCard item={cardData} />
      </Layout>
    </>
  );
}
