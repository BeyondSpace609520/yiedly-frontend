import Head from 'next/head';
import { Explorer } from '../components/explorer';
import { Layout } from '../components/layout';
export default function ExplorerPage() {
  return (
    <>
      <Head>
        <title>NFT Marketplace - Explorer</title>
        <link rel="icon" href="/img/YIELDLY_TOKEN.png" />
      </Head>
      <Layout>
        <Explorer />
      </Layout>
    </>
  );
}
