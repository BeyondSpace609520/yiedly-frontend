import React from 'react';
import Image from 'next/image';
import Link from 'next/link';
import styles from './Footer.module.scss';
import { IconInstagram, IconLinkedin, Icontwitter } from '../ui/Icons';
export const Footer: React.FC = () => {
  return (
    <div className="bg-black">
      <div className={styles.container}>
        <div className={`${styles.top} flex-wrap`}>
          <div className="flex flex-wrap">
            <div className={styles.item}>
              <div>
                <Link href="/" passHref>
                  About Yesports
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Marketplace
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Featured
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Categories
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Spaces
                </Link>
              </div>
            </div>
            <div className={styles.item}>
              <div>
                <Link href="/" passHref>
                  Become a Creator
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Artistic Guidlines
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Careers
                </Link>
              </div>
              <div>
                <Link href="/" passHref>
                  Help
                </Link>
              </div>
            </div>
          </div>
          <div className="flex flex-wrap">
            <div className={styles.item}>
              <h1>Newsletter</h1>
              <p>
                Stay up to date on new releases, interviews, events, and updates
                from NFTY’s community.
              </p>
            </div>
            <div className={styles.item_social}>
              <div className={styles.social}>
                <h1>Social</h1>
                <div className={styles.icon}>
                  <div>
                    <a href="#" target="_blank" rel="noreferrer">
                      <IconInstagram color={'#4E4E4E'}/>
                    </a>
                  </div>
                  <div>
                    <a href="#" target="_blank" rel="noreferrer">
                      <Icontwitter color={'#4E4E4E'}/>
                    </a>
                  </div>
                  <div>
                    <a href="#" target="_blank" rel="noreferrer">
                      <IconLinkedin />
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.bottom}>
          <div className={styles.leftSide}>
            <Link href="/" passHref>
              <a className={styles.logo}>
                <Image
                  src="/img/YieldlyFooter.png"
                  layout="fill"
                  objectFit="contain"
                  alt=""
                />
              </a>
            </Link>
            <Link href="/" passHref>
              <p>Terms of Service</p>
            </Link>
            <Link href="/" passHref>
              <p>Privacy</p>
            </Link>
          </div>
          <button className="linkBox footer-link">
            V1.2.2 © 2021 - Yieldly Limited
          </button>
        </div>
      </div>
    </div>
  );
};
