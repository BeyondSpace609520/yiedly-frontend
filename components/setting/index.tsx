import React, { useState } from 'react';
import styles from './Setting.module.scss';
import { EditInput } from '../ui/editInput';
import CoverUpload from '../ui/coverUpload';
import AvatarUpload from '../ui/avatarUpload';
import { IconReqire } from '../ui/Icons';

export const Setting: React.FC = () => {
  const [image, setImage] = useState<File | null>(null);
  const [imageType, setImageType] = useState<string>('image');
  const [errors, setErrors] = useState<object>({});
  const [buffer, setBuffer] = useState<string | ArrayBuffer | null>(null);
  const [avatar, setAvatar] = useState<File | null>(null);
  const [bufferAvatar, setBufferAvatar] = useState<string | ArrayBuffer | null>(null);
  const onAddImage = (item: File) => {
    console.log(item.type);
    if (item) setImageType(item.type.split('/')[0]);
    setErrors((prev) => ({
      ...prev,
      image: '',
    }));
    setImage(item);
    const reader = new FileReader();
    reader.onabort = () => console.log('file reading was aborted');
    reader.onerror = () => console.log('file reading has failed');
    reader.onload = () => {
      // Do whatever you want with the file contents
      const binaryStr = reader.result;
      setBuffer(binaryStr);
    };
    reader.readAsArrayBuffer(item);
  };
  const onAddAvatar = (item: File) => {
    console.log(item.type);
    if (item) setImageType(item.type.split('/')[0]);
    setErrors((prev) => ({
      ...prev,
      image: '',
    }));
    setAvatar(item);
    const reader1 = new FileReader();
    reader1.onabort = () => console.log('file reading was aborted');
    reader1.onerror = () => console.log('file reading has failed');
    reader1.onload = () => {
      // Do whatever you want with the file contents
      const binaryStr = reader1.result;
      setBufferAvatar(binaryStr);
    };
    reader1.readAsArrayBuffer(item);
  };
  const deleteAvatar = () => {
    const reader1 = new FileReader();
    reader1.abort();
    setAvatar(null);
  };
  const deleteImage = () => {
    const reader = new FileReader();
    reader.abort();
    setImage(null);
  };

  return (
    <div className={`dark:text-white mt-4 ${styles.container}`}>
      <div className={styles.header}>
        <span className="md:text-2xl text-xl font-black dark:text-white">Edit Profile</span>
        <div className="mt-4 max-w-full flex-shrink-0 flex-direction flex-col flex">
          <span className="text-lg text-edit">You can set preferred display name, create your branded profile URL and manage other personal settings</span>
        </div>
      </div>
      <div className="flex flex-col-reverse items-center md:items-start  md:flex-row md:flex-shrink-0">
        <div className={styles.forms}>
          <div className="mt-3  px-6 sm:px-8 mt-6">
            <h3 className="text-lg md:text-xl mb-5 font-semibold">
              Upload a Cover Image
            </h3>
            <CoverUpload
              imageType={imageType}
              image={image}
              setImage={onAddImage}
              deleteImage={deleteImage}
            />
          </div>
          <div className="flex mt-6 text-lg">
            <div className="max-w-full flex-col pl-6 sm:pl-8">
              <span>User Name</span>
              <EditInput placeholder="Enter your user name" />
            </div>
            <div className="max-w-full flex-col pr-6 sm:pr-8 sm:pl-2">
              <span>Nick Name</span>
              <EditInput prefix="@" placeholder="Nick Name" />
            </div>
          </div>
          <div className="max-w-full flex-col px-6 sm:px-8 mt-6 text-lg">
            <span>Custom URL</span>
            <EditInput placeholder="Enter your custom URL" />
          </div>
          <div className="max-w-full flex-col px-6 sm:px-8 mt-6 text-lg">
            <span>Bio</span>
            <EditInput placeholder="Tell about yourself in a few words" />
          </div>
          <div className="max-w-full flex-col px-6 sm:px-8 mt-6 text-lg">
            <span>Personal site or Portfolio</span>
            <EditInput prefix="https://" placeholder="Tell about yourself in a few words" />
          </div>
          <div className="flex justify-start px-6 sm:px-8 mt-6">
            <button className="flex-start px-8 py-4 rounded-full shadow-md font-bold text-white bg-blue-700 text-lg mt-4 hover:bg-blue-600">
              Save
            </button>
          </div>
        </div>
        <div className={styles.upload}>
          <AvatarUpload
            imageType={imageType}
            image={avatar}
            setAvatar={onAddAvatar}
            deleteAvatar={deleteAvatar}
          />
        </div>
      </div>
    </div>
  );
};
