export type TypeNFT = {
    tokenId: string;
    auctionInfo: TypeAuctionInfo | null;
    category: string;
    creator: TypeNFTOwner;
    owner: TypeNFTOwner;
    currencyType: string;
    title: string;
    description: string;
    image: string;
    imageType: string;
    isSale: boolean;
    price: number;
    tokenURI: string;
    created: number;
    bidHistory: TypeNFTHistory | object;
    totalHistory: TypeNFTHistory | object;
    followingCount: number;
    tags: string[];
}

export type TypeAuctionInfo = {
    tokenId: string;
    auctionCreator: TypeNFTOwner | null;
    lastBidder: TypeNFTOwner;
    auctionLength: number;
    auctionEndTime: number;
    currentPrice: number;
}

export type TypeNFTOwner = {
    id: string;
    addressAlgo: string[];
    addressPoly: string[];
    avatarImage: string;
    nickName: string;
    userName: string;
    bio: string;
    followerCount: number;
    followingCount: number;
}

export type TypeNFTHistory = {
    tokenId: string;
    walletAddress: string;
    walletType: string;
    user: TypeNFTOwner;
    time: number;
    actionType: string;
    price: number;
    priceUSD: number;
    transactionURL: string;
}

export type TypeTrait = { key: string; value: string }



export type TypeCurrency = {
    title: string;
    rate: number;
}

export const mockNftDatas: TypeNFT[] = [
    {
      tokenId: '1',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/1.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
    {
      tokenId: '2',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/2.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
    {
      tokenId: '3',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/3.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
    {
      tokenId: '4',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/4.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
    {
      tokenId: '5',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/5.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
    {
      tokenId: '6',
      auctionInfo: null,
      category: 'art',
      creator: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      owner: {
        id: '1',
        addressPoly: [
          '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        addressAlgo: [
          'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
        ],
        avatarImage: '/img/avatar.jpg',
        nickName: '@nickName',
        userName: 'User Name',
        bio: 'This is test user',
        followerCount: 4,
        followingCount: 2,
      },
      currencyType: 'ALGO',
      title: 'Test NFT',
      description: 'This is test NFT',
      image: '/img/nft/6.png',
      imageType: 'image',
      isSale: true,
      price: 5636,
      tokenURI:
        'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
      created: 1629971030309,
      followingCount: 0,
      bidHistory: {},
      totalHistory: {},
      tags: [
        '3d makeup',
        'digital makeup',
        'digital',
        'cosmic',
        'new dimensions',
      ],
    },
  ];
