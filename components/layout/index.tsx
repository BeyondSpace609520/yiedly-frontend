import React, { useContext } from 'react';
import dynamic from 'next/dynamic';

import { Footer } from '../footer';
import { ThemeContext } from '../../theme/ThemeProvider';

const HeaderWithNoSSR = dynamic(() => import('../header') as any, {
  ssr: false,
});

export const Layout: React.FC = ({ children }) => {
  const { theme, toggleTheme } = useContext(ThemeContext);
  return (
    <div className={theme}>
      <div className="dark:bg-body">
        <HeaderWithNoSSR />
        <div className="">{children}</div>
        <Footer />
      </div>
    </div>
  );
};
