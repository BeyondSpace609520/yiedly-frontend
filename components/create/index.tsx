/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import Switch from 'react-switch';
import Link from 'next/link';

import DatePicker from 'react-datepicker';
import { NFTStorage } from 'nft.storage';
import Web3 from 'web3';
import { AbiItem } from 'web3-utils';
import { parseUnits } from '@ethersproject/units';
import {
  NFTStorageKey,
  NFT_ADDRESS,
  NFT_MARKET_ADDRESS,
  infuraProvider,
} from '../../constant/index';
import ipfs from '../../utils/ipfsApi';
import NFT_INFO from '../../artifacts/contracts/Yieldly.sol/Yieldly.json';
import Market_INFO from '../../artifacts/contracts/Marketplace.sol/YieldlyMarketplace.json';
import { CustomDropdown } from '../ui/customDropdown';
import UploadArea from '../upload';
import 'react-datepicker/dist/react-datepicker.css';

import { toast } from 'react-toastify';
import moment from 'moment';
import {
  CHAIN_ID,
  CHAIN_ID_ERR_MSG,
  METAMASK_INSTALL_ERR_MSG,
} from '../../constant';
import { IconClose, IconReqire } from '../ui/Icons';
import { TypeTrait } from '../utils/interface';

const ethLists: string[] = ['ALGO', 'MATIC'];
const durations: string[] = ['12 hours', '1 day', '3 days', '5 days', '7 days'];
const durationsTime: any = {
  '12 hours': 12 * 3600000,
  '1 day': 24 * 3600000,
  '3 days': 3 * 24 * 3600000,
  '5 days': 5 * 24 * 3600000,
  '7 days': 7 * 24 * 3600000,
};
const collections: string[] = ['Collection1', 'Collection2', 'Collection3'];
const categories: string[] = [
  'Art',
  'Music',
  'Film',
  'Sports',
  'Education',
  'Photography',
  'Games',
  'Other',
];

const client = new NFTStorage({ token: NFTStorageKey });

export const Create: React.FC = () => {
  const [image, setImage] = useState<File | null>(null);
  const [imageType, setImageType] = useState<string>('image');
  const [errors, setErrors] = useState<object>({});
  const [buffer, setBuffer] = useState<string | ArrayBuffer | null>(null);
  const [putOnSale, setPutOnSale] = useState<boolean>(false);
  const [minPrice, setMinPrice] = useState<string>('0');
  const [currency, setCurrency] = useState<string>(ethLists[0]);
  const [startDate, setStartDate] = useState<Date>(new Date());
  const [duration, setDuration] = useState<string>(durations[0]);
  const [title, setTitle] = useState<string>('');
  const [description, setDescription] = useState<string>('');
  const [royalties, setRoyalties] = useState<string>('0');
  const [category, setCategory] = useState<string>(categories[0]);
  const [collection, setCollection] = useState<string>(collections[0]);
  const [tags, setTags] = useState<string[]>([]);
  const [isProcessing, setIsProcessing] = useState<boolean>(false);
  const [traits, setTraits] = useState<TypeTrait[]>([]);
  const [traitKey, setTraitKey] = useState<string>('');
  const [traitValue, setTraitValue] = useState<string>('');

  const [openTraitModal, setOpenTraitModal] = useState<boolean>(false);
  const [selectedTrait, setSelectedTrait] = useState<TypeTrait | null>(null);

  const onAddImage = (item: File) => {
    console.log(item.type);
    if (item) setImageType(item.type.split('/')[0]);
    setErrors((prev) => ({
      ...prev,
      image: '',
    }));
    setImage(item);
    const reader = new FileReader();
    reader.onabort = () => console.log('file reading was aborted');
    reader.onerror = () => console.log('file reading has failed');
    reader.onload = () => {
      // Do whatever you want with the file contents
      const binaryStr = reader.result;
      setBuffer(binaryStr);
    };
    reader.readAsArrayBuffer(item);
  };

  const deleteImage = () => {
    const reader = new FileReader();
    reader.abort();
    setImage(null);
  };

  const mintNFT = async () => {
    if (!buffer) {
      toast.error('Please select file of NFT');
      return;
    }
    if (!title) {
      toast.error('Please insert the title of NFT');
      return;
    }
    if (!description) {
      toast.error('Please insert the description of NFT');
      return;
    }
    if (!startDate) {
      toast.error('Please select the start date of auction');
      return;
    }
    if (!duration) {
      toast.error('Please select the duration of auction');
      return;
    }

    if (typeof window != 'undefined' && window.hasOwnProperty('web3')) {
      const wa = window as any;
      if ((await wa.web3.eth.getChainId()) !== CHAIN_ID) {
        toast.error(CHAIN_ID_ERR_MSG);
      }
      const account = await wa.web3.eth.getAccounts();
      setIsProcessing(true);

      const data = await ipfs.add(buffer);
      const cid = await client.storeDirectory([
        new File(
          [
            JSON.stringify({
              title: title,
              description: description,
              creator: account[0],
              royalties: royalties,
              image: `https://ipfs.io/ipfs/${data.path}`,
            }),
          ],
          'metadata.json',
        ),
      ]);

      if (cid) {
        const tokenURI = `https://ipfs.io/ipfs/${cid}/metadata.json`;

        const provider = new Web3.providers.HttpProvider(infuraProvider);
        const web3infura = new Web3(provider);
        const nftContract = new web3infura.eth.Contract(
          NFT_INFO.abi as AbiItem[],
          NFT_ADDRESS,
        );
        const marketplaceContract = new web3infura.eth.Contract(
          Market_INFO.abi as AbiItem[],
          NFT_MARKET_ADDRESS,
        );

        const isApproved = await nftContract.methods
          .isApprovedForAll(account[0], NFT_MARKET_ADDRESS)
          .call();
        if (!isApproved) {
          await nftContract.methods.setApprovalForAll(NFT_MARKET_ADDRESS, true);
        }
        const auction_length = parseInt(duration) * 3600;
        marketplaceContract.methods
          .mint(
            tokenURI,
            parseUnits(minPrice.toString()),
            putOnSale,
            royalties,
            moment(startDate).format('X'),
            auction_length,
          )
          .call({ from: account[0] })
          .then(() => {
            console.log('--success--');
          })
          .catch((err: any) => {
            console.log(err);
          });

        setIsProcessing(false);
      } else {
        toast.error('Uploading failed');
        setIsProcessing(false);
      }
    } else {
      toast.error(METAMASK_INSTALL_ERR_MSG);
    }
  };

  const inputTags = (event: any) => {
    const code = event.code;
    if (!['Comma', 'Enter', 'Backspace'].includes(code)) return;
    const target = event.target;
    const tagName = target.value;
    if (!tagName && tagName === ',') {
      target.value = '';
      return;
    }
    if (code !== 'Backspace' && tags.includes(tagName)) {
      event.preventDefault();
      return;
    }
    const newTags = tags;
    if (!tagName) {
      target.value = '';
      if (code === 'Backspace') {
        setTags(newTags.slice(0, newTags.length - 1));
      }
      return;
    } else if (code === 'Backspace') return;
    event.preventDefault();
    setTags([...newTags, tagName]);
    target.value = '';
  };

  return (
    <div className="max-w-screen-xl mx-auto py-16 dark:text-white px-6 xl:px-16">
      <div className="flex flex-col lg:flex-row">
        <div className="w-full lg:w-1/2 lg:pr-5 mb-10">
          <UploadArea
            imageType={imageType}
            image={image}
            setImage={onAddImage}
            deleteImage={deleteImage}
          />
        </div>
        <div className="w-full lg:w-1/2 lg:pl-5 flex flex-col justify-between mb-10">
          <div className="mb-10">
            <h3 className="text-lg md:text-2xl mb-12">Create new NFT</h3>
            <p className="text-sm md:text-md mb-2">
              Image, Video, Audio, or 3D Model
            </p>
            <p className="text-sm md:text-md">
              File types supported: JPG, PNG, GIF, SVG, MP4, WEBM, MP3, WAV,
              OGG, GLB, GLTF. Max size: 40 MB
            </p>
          </div>
          <div className="form-group text-md md:text-lg mt-auto">
            <p className="mb-4">NFT Type</p>
            <div className="form-input z-30">
              <CustomDropdown
                className="w-full text-white-important bg-transparent-important"
                selected={category}
                lists={categories}
                handleSelect={(item) => {
                  setCategory(item);
                }}
              />
            </div>
          </div>
        </div>
      </div>

      {/* Title */}
      <div className="form-group mb-10 text-md md:text-lg">
        <p className="mb-4">Title*</p>
        <div className="form-input">
          <input
            type="text"
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
          />
        </div>
      </div>

      {/* Description */}
      <div className="form-group mb-10 text-md md:text-lg">
        <p className="mb-4">Description*</p>
        <div className="form-input">
          <input
            type="text"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
          />
        </div>
      </div>

      <div className="pt-4 pb-16 border-t border-b border-main dark:border-create">
        <div className="flex justify-between items-center mb-16">
          <h3 className="text-lg md:text-xl font-semibold">
            Put on marketplace
          </h3>
          <Switch
            onColor="#7200FF"
            onChange={() => {
              setPutOnSale(!putOnSale);
            }}
            checked={putOnSale}
            height={24}
          />
        </div>

        {putOnSale && (
          <>
            <div className="flex flex-wrap">
              {/* Minimum bid */}
              <div className="form-group text-md w-full md:text-lg xl:w-1/2 xl:pr-3 mb-16">
                <p className="mb-4">Minimum Bid</p>
                <div className="form-input">
                  <input
                    type="text"
                    placeholder="Enter minimum bid"
                    value={minPrice}
                    onChange={(e) => {
                      if (/^(\d+)?(.(\d+)?)?$/.test(e.target.value))
                        setMinPrice(e.target.value);
                    }}
                  />
                  <CustomDropdown
                    className="text-white-important bg-transparent-important"
                    selected={currency}
                    lists={ethLists}
                    handleSelect={(item) => {
                      setCurrency(item);
                    }}
                  />
                </div>
              </div>
              <div className="form-group w-full xl:w-1/2 xl:pl-3 flex flex-wrap">
                {/* Starting date */}
                <div className="form-group text-md md:text-lg w-full md:w-1/2 md:pr-3 mb-16">
                  <p className="mb-4">Starting date</p>
                  <div className="form-input">
                    <DatePicker
                      selected={startDate}
                      className="text-md py-0 w-full"
                      onChange={(e) => {
                        setStartDate(e as Date);
                      }}
                      showTimeSelect
                      timeFormat="HH:mm"
                      timeIntervals={20}
                      timeCaption="time"
                      dateFormat="MMMM d, yyyy h:mm aa"
                    />
                  </div>
                </div>

                {/* Duration */}
                <div className="form-group text-md md:text-lg w-full md:w-1/2 md:pl-3 mb-16">
                  <p className="mb-4">Duration</p>
                  <div className="form-input z-20">
                    <CustomDropdown
                      className="w-full text-white-important bg-transparent-important"
                      selected={duration}
                      lists={durations}
                      handleSelect={(item) => {
                        setDuration(item);
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </>
        )}

        {/* Royalty and Collection */}

        <div className="flex flex-wrap">
          <div className="form-group text-md md:text-lg w-full lg:w-1/2 lg:pr-3 lg:order-1">
            <div className="flex mb-4">
              <p className="w-5/12">Royalties</p>
              <span className="w-5/12 text-sm">
                Suggested: 0%, 10%, 20%, 30%. Maximum is 50%
              </span>
            </div>
          </div>
          <div className="form-group text-md md:text-lg w-full lg:w-1/2 lg:pr-3 lg:order-3 mb-16">
            <div className="form-input flex items-center pr-5">
              <input
                type="text"
                placeholder='e.g. "Yesport logo"'
                value={royalties}
                onChange={(e) => {
                  if (/^(\d+)?$/.test(e.target.value))
                    setRoyalties(e.target.value);
                }}
              />
              <span>%</span>
            </div>
          </div>
          <div className="form-group text-md md:text-lg w-full lg:w-1/2 lg:pl-3 lg:order-2">
            <div className="flex mb-4">
              <p className="w-5/12">Collection</p>
              <span className="w-5/12 text-sm">
                Want to release this as a Collection?
                <br />
                <br />
                If you dont, skip this. You can always release a collection
                later on.
              </span>
            </div>
          </div>
          <div className="form-group text-md md:text-lg w-full lg:w-1/2 lg:pl-3 lg:order-4 mb-16">
            <div className="form-input z-10 flex items-center">
              <CustomDropdown
                className="w-full text-white-important bg-transparent-important"
                selected={collection}
                lists={collections}
                handleSelect={(item) => {
                  setCollection(item);
                }}
              />
            </div>
          </div>
        </div>

        {/* Tags */}

        <div className="form-group mb-10 text-md md:text-lg">
          <p className="mb-4">Tags</p>
          <div className="form-input p-5 gap-5 flex-wrap justify-start">
            {tags &&
              tags.map((tag: string) => (
                <span key={tag} className="bg-btn-main">
                  {tag}
                </span>
              ))}

            <input
              className="input-el"
              placeholder="Add Tag +"
              contentEditable="true"
              onKeyDown={(e) => {
                inputTags(e);
              }}
            />
          </div>
        </div>

        {/* Traits */}
        <div className="form-group mb-10 text-md md:text-lg">
          <p className="mb-4">Traits</p>
          <div className="flex flex-wrap">
            {traits &&
              traits.map((trait: TypeTrait) => (
                <div
                  className="form-input trait-item flex-col px-2 pt-4 pb-2 mr-3 gap-5 flex-wrap justify-start"
                  key={trait.key}
                  // onClick={() => {
                  //   setTraitKey(trait.key);
                  //   setTraitValue(trait.value);
                  //   setOpenTraitModal(true);
                  // }}
                >
                  <p
                    className="absolute top-2 right-2"
                    onClick={() => {
                      setTraits(traits.filter((x) => x.key !== trait.key));
                    }}
                  >
                    <IconClose scale={50} />
                  </p>
                  <p className="text-sm mb-0">{trait.key}</p>
                  <p className="bg-btn-main size-auto w-full">{trait.value}</p>
                </div>
              ))}
            <div className="form-input p-5 gap-5 flex-wrap justify-start">
              <div
                className="text-gray-400"
                onClick={() => {
                  setSelectedTrait(null);
                  setOpenTraitModal(true);
                }}
              >
                Add Trait +
              </div>
            </div>
          </div>
          {openTraitModal && (
            <>
              <div className="overflow-x-hidden overflow-y-auto fixed inset-0 z-50 flex outline-none focus:outline-none justify-center items-center">
                <div className="relative w-auto my-6 mx-auto max-w-2xl">
                  <div className="border-0 rounded-3xl shadow-lg relative flex flex-col w-full bg-white dark:bg-body outline-none focus:outline-none py-4 px-8">
                    <div className="flex items-start justify-between p-5 border-b border-create rounded-t">
                      <h3 className="text-2xl text-center w-full">Add Trait</h3>
                      <button
                        className="p-1 ml-auto bg-transparent text-gray-300 float-right text-xl leading-none font-semibold outline-none focus:outline-none"
                        onClick={() => {
                          setOpenTraitModal(false);
                        }}
                      >
                        <IconClose />
                      </button>
                    </div>
                    <div className="relative p-6 flex-auto">
                      <p className="my-4 text-md leading-relaxed text-center">
                        Traits show up underneath your item, are clickable, and
                        can be filtered in your collection’s sidebar
                      </p>
                    </div>
                    <div className="flex items-center justify-end py-6 border-t border-create rounded-b mb-4">
                      <div className="w-1/2 pr-3">
                        <p className="pl-4 mb-3 text-sm">Trait</p>
                        <div className="form-input">
                          <input
                            type="text"
                            value={traitKey}
                            onChange={(e) => {
                              setTraitKey(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                      <div className="w-1/2 pl-3">
                        <p className="pl-4 mb-3 text-sm">Description</p>
                        <div className="form-input">
                          <input
                            type="text"
                            value={traitValue}
                            onChange={(e) => {
                              setTraitValue(e.target.value);
                            }}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="mb-5">
                      <button
                        className="bg-btn-main size-auto w-full mb-4"
                        type="button"
                        onClick={() => {
                          if (
                            traits.filter((x: TypeTrait) => x.key === traitKey)
                              .length > 0
                          )
                            toast.error(
                              'The key of trait is alreay used. Please update that trait.',
                            );
                          else {
                            traits.push({ key: traitKey, value: traitValue });
                            setTraitKey('');
                            setTraitValue('');
                            setOpenTraitModal(false);
                          }
                        }}
                      >
                        Save
                      </button>
                      <button
                        className="linkBox size-auto py-3 w-full"
                        type="button"
                        onClick={() => {
                          setTraitKey('');
                          setTraitValue('');
                          setOpenTraitModal(false);
                        }}
                      >
                        Cancel
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="flex opacity-25 fixed inset-0 z-40 bg-white"
                onClick={() => {
                  console.log('sss');
                }}
              ></div>
            </>
          )}
        </div>
      </div>
      <button
        className="bg-btn-main w-full text-xl mt-16"
        onClick={() => {
          mintNFT();
        }}
      >
        Create
      </button>
    </div>
  );
};
