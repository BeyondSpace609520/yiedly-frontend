import React, { useState } from 'react';
import Image from 'next/image';
import styles from './CollectionCard.module.scss';

export declare type CollectionData = {
  collection: string;
  img: string;
  creatorName: string;
};

declare type PageProps = {
  item: CollectionData;
  sizeMd?: boolean;
};

export const CollectionCard = ({ item, sizeMd }: PageProps) => {
  return (
    <div className={`${styles.nftCard} ${sizeMd ? styles.sizeMd : ''}`}>
      <div className={styles.cardImg}>
        <Image
          src={item.img}
          layout="fill"
          objectFit="cover"
          alt={item.collection}
        />
      </div>
      <div className={styles.info}>
        <h3>{item.collection}</h3>
        <p>
          <span></span> {item.creatorName}
        </p>
      </div>
    </div>
  );
};
