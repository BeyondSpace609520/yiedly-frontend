import React, { useState } from 'react';
import Image from 'next/image';
import styles from './Card.module.scss';
import { Nickname } from './nickname';
import { BidBtn } from '../bidBtn';
import { IconHeart, IconShare } from '../Icons';
import { TypeNFT } from '../../utils/interface';

declare type PageProps = {
  item: TypeNFT;
};

export const Card = ({ item }: PageProps) => {
  return (
    <div className={styles.nftCard}>
      <div className={styles.cardImg}>
        {item.imageType === 'image' ? (
          typeof item.image !== 'string' ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img src={URL.createObjectURL(item.image)} alt="NFT Image" />
          ) : (
            <Image
              src={item.image as string}
              layout="fill"
              objectFit="cover"
              alt={item.description}
            />
          )
        ) : item.imageType === 'video' ? (
          <video controls loop muted width="100%">
            <source src={URL.createObjectURL(item.image)} />
          </video>
        ) : (
          <audio controls>
            <source src={URL.createObjectURL(item.image)} />
          </audio>
        )}
      </div>
      <div className="flex flex-col px-6 py-5 text-white bg-card">
        <h3 className="mb-4 text-lg">{item.title}</h3>
        <div className="flex items-center">
          <Nickname
            avatar={item.owner.avatarImage}
            nickname={item.owner.nickName}
          />
        </div>
      </div>
      <div className="px-6 pt-4 pb-6 text-white bg-card border-t border-gray-500">
        <div className="flex justify-between text-sm text-white text-opacity-30 mb-4">
          <span>Current Bid</span>
          <span>Ending in</span>
        </div>
        <div className="flex justify-between text-md mb-6">
          <span>
            {item.price} {item.currencyType}
          </span>
          <span>{'1h 39m 05s'}</span>
        </div>
        <div className="flex justify-between">
          <BidBtn />
          <div className="flex items-center">
            <a href="#" className="mr-2">
              <IconHeart size={20} />
            </a>
            <a href="#">
              <IconShare size={20} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
