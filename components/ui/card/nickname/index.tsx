import React from 'react';
import Image from 'next/image';
import styles from './Nickname.module.scss';

declare type PageProps = {
  avatar: string;
  nickname: string;
};

export const Nickname = ({ avatar, nickname }: PageProps) => {
  return (
    <>
      <div className={styles.nickName}>
        <div className="relative w-10 h-10 rounded-full mr-3">
          <Image
            src={avatar}
            className="rounded-full"
            layout="fill"
            objectFit="fill"
            alt=""
          />
        </div>

        <p>{nickname}</p>
      </div>
    </>
  );
};
