import React from 'react';
import Image from 'next/image';

import styles from './AvatarUpload.module.scss';
import { IconClose } from '../../ui/Icons';

declare type PageProps = {
  imageType: string;
  image: File | null;
  setAvatar: (item: File) => void;
  deleteAvatar: () => void;
};

const AvatarUpload = ({ imageType, image, setAvatar, deleteAvatar }: PageProps) => {
  return (
    <>
      <div className={`md:mt-24 mt-8 ${styles.avatarContainer}`}>
        {image ?
          <div className={styles.avatarArea} style={{ backgroundImage: `url(${URL.createObjectURL(image)})` }}>
            <div className={styles.btnRemove} onClick={deleteAvatar}>
              <IconClose />
            </div>
          </div>
          : <div className={styles.avatarArea} ></div>}
        <p className="text-gray-500 text-center mt-2">We recommend an image of at least 400x400.<br/>Gifs work too</p>
      </div>
      <div className={styles.forUpload}>
        <div
          className={`${styles.btnChoose} bg-choose dark:bg-choose-dark rounded-full`}
        >
          <input
            type="file"
            id="avatar"
            accept={'.jpg, .jpeg, .png, .gif'}
            onChange={(e) => {
              if (e.target.files && e.target.files?.length > 0)
                setAvatar(e.target.files[0]);
            }}
          />
          {!image && <label htmlFor="avatar">Choose file...</label>}
        </div>
      </div>
    </>
  );
};

export default AvatarUpload;
