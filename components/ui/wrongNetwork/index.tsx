import React from 'react';
import { CHAIN_ID_ERR_MSG } from '../../../constant';

export const WrongNetwork = () => {
  return (
    <div className="fixed inset-0 z-50 flex flex-col justify-center items-center bg-gray-900 bg-opacity-70">
      <div className="animate-spin rounded-full h-32 w-32 border-t-4 border-b-4 border-purple-500"></div>
      <p className="mt-12 text-xl text-white drop-shadow-lg">
        {CHAIN_ID_ERR_MSG}
      </p>
    </div>
  );
};
