import StyledModal from '../styledModal';
import { CRYPTO_TYPE } from '../../../constant';

interface ConnectWalletModalProps {
  open: boolean;
  handleMetaMaskConnect: () => void;
  handleMyAlgoConnect: () => void;
  setCryptoType: (type: string) => void;
}

const ConnectWalletModal = ({
  open,
  handleMetaMaskConnect,
  handleMyAlgoConnect,
  setCryptoType,
}: ConnectWalletModalProps) => {
  return (
    <StyledModal open={open}>
      <div className="grid gap-2">
        <button
          className="p-2 pl-5 pr-5 bg-transparent border-2 border-indigo-500 text-indigo-500 text-lg rounded-lg transition-colors duration-700 transform hover:bg-indigo-500 hover:text-gray-100 focus:border-4 focus:border-indigo-300"
          onClick={() => {
            setCryptoType(CRYPTO_TYPE.ETH);
            handleMetaMaskConnect();
          }}
        >
          Connect with MetaMask
        </button>
        <button
          className="p-2 pl-5 pr-5 bg-transparent border-2 border-indigo-500 text-indigo-500 text-lg rounded-lg transition-colors duration-700 transform hover:bg-indigo-500 hover:text-gray-100 focus:border-4 focus:border-indigo-300"
          onClick={() => {
            handleMyAlgoConnect();
            setCryptoType(CRYPTO_TYPE.ALGO);
          }}
        >
          Connect with My Algo
        </button>
      </div>
    </StyledModal>
  );
};

export default ConnectWalletModal;
