import React, { useContext, useState } from 'react';

declare type PageProps = {
  children: any;
  title: string;
};

export const FilterExpand = ({ title, children }: PageProps) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);
  return (
    <div className="border-t border-body pl-10 pr-8">
      <div
        className="flex justify-between items-center py-8 cursor-pointer"
        onClick={() => {
          setIsOpen(!isOpen);
        }}
      >
        <p className="text-lg">{title}</p>
        <svg
          width="18"
          height="10"
          viewBox="0 0 18 10"
          className={!isOpen ? 'transform rotate-180' : ''}
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M17 9L9 1L1 9"
            stroke="#E2E2E2"
            strokeWidth="2"
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </div>
      {isOpen && <div className="mb-8">{children}</div>}
    </div>
  );
};
