import React from 'react';
import styles from './EditInput.module.scss';

export const EditInput = ({ prefix, placeholder }: { prefix?: string, placeholder?: string }) => {
  return (
    <div className={`dark:border-b-500 dark:text-white ${styles.container}`}>
      {prefix &&
        <div className={styles.prefix}>
          {prefix}
        </div>
      }
      <div className={styles.wrapInput}>
        <input type="text" placeholder={placeholder} />
      </div>
    </div>
  );
};
