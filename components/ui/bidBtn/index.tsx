import React from 'react';
import styles from './BidBtn.module.scss';

declare type BtnProps = {
  sizeMd?: boolean;
  className?: string;
};

export const BidBtn = ({ sizeMd, className }: BtnProps) => {
  return (
    <div
      className={`${styles.bidBtn} ${sizeMd ? styles.sizeMd : ''} ${className}`}
    >
      <span className={`text-white ${sizeMd ? 'text-md' : 'text-sm'}`}>
        PLACE A BID
      </span>
    </div>
  );
};
