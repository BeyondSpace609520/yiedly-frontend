import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import axios from 'axios';

import styles from './Card.module.scss';
import { BidBtn } from '../bidBtn';
import { IconHeart, IconShare } from '../Icons';

const approvalProgram =
  'BSAJAAEECCAoAzCgjQYmAQAxIDIDEkQxBjIDEkQiNQAxGEEFJChkSSJbNQFJJVs1AlcQIDUDMRshBhJENhoAF0kiEkEBFUgiNAESRDYaARdJIhJMNAISEUQoSDYaAkkVIQcSRElXACA1/0khBFs1/kkhBVs1/UgxADIJEkQ0/zUDIQhJQQA0NABJIwg1AExLATgIEkQjSwE4EBJEIksBOAESRDIDSwE4BhJEMgNLATggEkQ0A0sBOAcSREghCDQASSMINQBMSwE4CBJEI0sBOBASRCJLATgBEkQyA0sBOAYSRDIDSwE4IBJENANLATgHEkRIIjQASSMINQBMSwE4EhJENP5LATgREkQkSwE4EBJEIksBOAESRDIDSwE4BhJEMgNLATggEkQ0A0sBOAASRDQDSwE4FBJESDEANP4WUDT9FlAjr0sBVwAwZ0gjNQEyBjUCMRkiEkRCA8pJIxJBAKhIIzQBEkQ2GgEXSSISTDQCEhFEI69kSVcAIDX/SSEEWzX+SSEFWzX9SDYaAkkVIhJESCNJQQA8NABJIwg1AExLATgSEkQ0/ksBOBESRCRLATgQEkQiSwE4ARJEMgNLATgGEkQyA0sBOCASRDQDSwE4FBJESDT/MQASRDT/NP4WUCWvNP0WUDT/UCMWUQcIUDIGFlAyBxZQJa9QgAgAAAAAAAAAAVBCAY1JIQYSQQGESCQ0ARJENhoBF0kiEkw0AhIRRCOvZElXACA1/0khBFs1/kkhBVs1/UkhB1s1/ElXOCA1+0mBWFs1+kmBYFs1+UmBaFs1+Eg2GgJJFYEJEkRJNfdINPciVSISQQDhNPdXAQhJNfYXNfU09UlBADQ0AEkjCDUATEsBOAgSRCNLATgQEkQiSwE4ARJEMgNLATgGEkQyA0sBOCASRDQDSwE4BxJESDT1NP0NRDT9SUEAPDQASSMINQBMSwE4CBJEI0sBOBASRCJLATgBEkQyA0sBOAYSRDIDSwE4IBJENANLATgAEkQ0+0sBOAcSREiAEAAAAAAAAAEFAAAAAAAAAAGwIzT/NP4WUDT1FjT8SYHYBAg0+oGsAgg0/A1NFlAxAFAjFlEHCFAyBhZQMgcWUDT5NPUINP0JFlA0+BZQQgBINPciVSMSQQA9gBAAAAAAAAABNAAAAAAAAAABsCM0/zT+FlA0/RY0/BZQNPtQIhZRBwhQMgYWUDIHFlA0+RZQNPgWUEIAAiJESSJbNf9JJVs1/klXECA1/UlXMAEXNfxJgTFbNftJgTlbNfpJgUFbNflJgUlbNfhISVcAIDX3SSEEWzX2SDT8QQA1NPc09hZQNP8WUDT+FlA0/VA0+hZQNPkWUDT4FlAjr0sBVwBwZ0gkNQEyBjUCMRkiEkRCARg0+ElBAEQ0AEkjCDUATEsBOBISRDT2SwE4ERJEJEsBOBASRCJLATgBEkQyA0sBOAYSRDIDSwE4IBJENANLATgAEkQ0/UsBOBQSREg0+UlBADw0AEkjCDUATEsBOAgSRCNLATgQEkQiSwE4ARJEMgNLATgGEkQyA0sBOCASRDQDSwE4ABJENPdLATgHEkRIIjQASSMINQBMSwE4EhJENPZLATgREkQkSwE4EBJEIksBOAESRDIDSwE4BhJEMgNLATggEkQ0A0sBOAASRDIJSwE4FRJESCI0AEkjCDUATEsBOAgSRCNLATgQEkQiSwE4ARJEMgNLATgGEkQyA0sBOCASRDQDSwE4ABJEMglLATgJEkRIMRmBBRJEKDQBFjQCFjQDUFBnNABJSSMIMgQSRDEWEkSB6AcLMQEORCNDMRkiEkQiNQEiNQIyAzUDQv/K';

const getApplicationInfo = async (appId: string) => {
  return (
    await axios.get(
      `https://testnet.algoexplorerapi.io/v2/applications/${appId}`,
    )
  )?.data;
};

declare type AuctionCardProps = {
  item: {
    appId: string;
    currencyType: string;
    title: string;
    description: string;
    image: string;
    price: string;
  };
};

const AuctionCard = ({ item }: AuctionCardProps) => {
  useEffect(() => {
    (async () => {
      let addr = item.appId;
      let accInfo = await getApplicationInfo(addr);
      console.log(accInfo);
    })();
  }, []);

  return (
    <div className={styles.nftCard}>
      <div className={styles.cardImg}>
        <Image
          src={item.image as string}
          layout="fill"
          objectFit="cover"
          alt={item.description}
        />
      </div>
      <div className="flex flex-col px-6 py-5 text-white bg-card">
        <h3 className="mb-4 text-lg">{item.title}</h3>
      </div>
      <div className="px-6 pt-4 pb-6 text-white bg-card border-t border-gray-500">
        <div className="flex justify-between text-sm text-white text-opacity-30 mb-4">
          <span>Current Bid</span>
          <span>Ending in</span>
        </div>
        <div className="flex justify-between text-md mb-6">
          <span>
            {item.price} {item.currencyType}
          </span>
          <span>{'1h 39m 05s'}</span>
        </div>
        <div className="flex justify-between">
          <BidBtn />
          <div className="flex items-center">
            <a href="#" className="mr-2">
              <IconHeart size={20} />
            </a>
            <a href="#">
              <IconShare size={20} />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AuctionCard;
