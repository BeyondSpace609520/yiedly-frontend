import React from 'react';
import Image from 'next/image';

import styles from './CoverUpload.module.scss';
import { IconClose } from '../../ui/Icons';

declare type PageProps = {
  imageType: string;
  image: File | null;
  setImage: (item: File) => void;
  deleteImage: () => void;
};

const CoverUpload = ({ imageType, image, setImage, deleteImage }: PageProps) => {
  return (
    <div className="border border-gray-400 border-dashed rounded-3xl relative">
      {image ? (
        <div className={styles.uploadedImage}>
          {imageType === 'image' ? (
            // eslint-disable-next-line @next/next/no-img-element
            <img src={URL.createObjectURL(image)} alt="nft" />
          ) : imageType === 'video' ? (
            <video controls loop muted width="100%">
              <source src={URL.createObjectURL(image)} />
            </video>
          ) : (
            <audio controls>
              <source src={URL.createObjectURL(image)} />
            </audio>
          )}
          <div className={styles.btnRemove} onClick={deleteImage}>
            <IconClose />
          </div>
        </div>
      ) : (
        <div className={styles.forUpload}>
          <p>Recommended size: 1500 x 500px</p>
          <p>PNG, GIF, JPG</p>
          <p>10MB max size</p>
          <div
            className={`${styles.btnChoose} bg-choose dark:bg-choose-dark rounded-full`}
          >
            <input
              type="file"
              id="upload"
              accept={'.jpg, .jpeg, .png, .mp4, .mpeg, .mp3, .wav'}
              onChange={(e) => {
                if (e.target.files && e.target.files?.length > 0)
                  setImage(e.target.files[0]);
              }}
            />
            <label htmlFor="upload">Choose file...</label>
          </div>
        </div>
      )}
    </div>
  );
};

export default CoverUpload;
