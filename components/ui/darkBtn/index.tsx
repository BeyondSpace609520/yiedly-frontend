import React from 'react';
import styles from './DarkBtn.module.scss';

export const DarkBtn = ({ nickname }: { nickname: string }) => {
  return (
    <div className={styles.nickName}>
      <p>{nickname}</p>
    </div>
  );
};
