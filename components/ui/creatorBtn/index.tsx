import React from 'react';
import Image from 'next/image';
import styles from './CreatorBtn.module.scss';

declare type PageProps = {
  avatar: string;
  nickname: string;
};

export const CreatorBtn = ({ avatar, nickname }: PageProps) => {
  return (
    <button className="creator">
      <div className="relative w-10 h-10 rounded-full mr-3">
        <Image
          src={avatar}
          className="rounded-full"
          layout="fill"
          objectFit="contain"
          alt=""
        />
      </div>
      <div className={styles.creatorName}>{nickname}</div>
    </button>
  );
};
