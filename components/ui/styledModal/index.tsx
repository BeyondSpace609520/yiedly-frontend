import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';

interface StyledModalProps {
  open: boolean;
}

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
    width: 'max-content',
    height: 'max-content',
  },
};
Modal.setAppElement('#__next');

const StyledModal: React.FC<StyledModalProps> = ({ open, children }) => {
  const [modalIsOpen, setIsOpen] = useState(false);

  useEffect(() => {
    setIsOpen(open);
  }, [open]);

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <Modal
      isOpen={modalIsOpen}
      onRequestClose={closeModal}
      contentLabel="Example Modal"
      style={customStyles}
    >
      <div className="px-4 py-2 flex justify-end	">
        <button onClick={closeModal} className="text-black">
          X
        </button>
      </div>
      <div className="p-3">{children}</div>
    </Modal>
  );
};

export default StyledModal;
