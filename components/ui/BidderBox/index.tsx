import React from 'react';
import { TypeNFTHistory } from '../../utils/interface';
import Image from 'next/image';
import styles from './BidderBox.module.scss';
import moment from 'moment';
import { IconExpand } from '../Icons';

declare type BidderProps = {
  bidHistory: TypeNFTHistory;
};

export const BidderBox = ({ bidHistory }: BidderProps) => {
  return (
    <div
      className={`linkBox justify-start history-card flex flex-col ${styles.bidderBox}`}
    >
      <div className="">
        <Image
          src={bidHistory.user.avatarImage}
          className="mb-8"
          width="30px"
          height="30px"
          alt=""
        />
        <p className="text-base text-card dark:text-gray-200 font-bold">
          Bid placed by
        </p>
        <h4 className="mb-4">{bidHistory.user.nickName}</h4>
        <h5 className="mb-5 text-base font-bold">
          {`${moment(bidHistory.time).format('MMM d, YYYY')} at ${moment(
            bidHistory.time,
          ).format('H:mma')}`}
        </h5>
      </div>
      <div className="border-t w-full border-main flex flex items-end justify-between">
        <div className="pt-5">
          <p className="mb-4 text-base text-card dark:text-gray-200 font-bold">
            {bidHistory.price} {bidHistory.walletType}
          </p>
          <h5 className="font-bold text-base">${bidHistory.priceUSD}</h5>
        </div>
        <IconExpand />
      </div>
    </div>
  );
};
