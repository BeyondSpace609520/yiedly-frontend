import React, { useEffect, useState } from 'react';
import Image from 'next/image';
import styles from './Trending.module.scss';
import { Algo, IconHeart, IconShare } from '../ui/Icons';
import { BidBtn } from '../ui/bidBtn';
import { CustomDropdown } from '../ui/customDropdown';
import { Card } from '../ui/card';
import { CreatorBtn } from '../ui/creatorBtn';
import { mockNftDatas, TypeNFT } from '../utils/interface';

type Props = {
  isLight?: boolean;
  trendingImg?: string;
  currentBid?: number;
  creator?: string;
  title?: string;
  avatar?: string;
  description?: string;
};

const trendingLists: string[] = ['Most Recent', 'Most Like', 'Least Like'];
export const Trending: React.FC<Props> = ({
  isLight,
  trendingImg = '/img/nft-1 1.png',
  currentBid = 5636,
  avatar = '/img/avatar.jpg',
  creator = 'FACT Revolution',
  title = 'BRAVE NEW WORLD #003',
  description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nam hendrerit nunc ac lacus sagittis ornare. Donec augue arcu, placerat at nibh vel, bibendum bibendum nulla. Donec nec diam pretium.<br/><br/>is volutpat egestas.Phasellus semper sit amet nisl dignissim volutpat.Aenean euismod pulvinar.\
  <br/><br/>Hand Draw and Digitized Art<br/><br/>1920x1920 px`,
}: Props) => {
  const [trending, setTrending] = useState<string>(trendingLists[0]);
  const [cards, setCards] = useState<TypeNFT[]>([]);

  useEffect(() => {
    setCards(mockNftDatas.slice(0, 4));
  }, []);
  return (
    <div className="dark:text-white">
      <div
        className={`flex justify-between items-center my-11 border-t border-main dark:border-opacity-30 py-11 ${styles.maxWidth}`}
      >
        <h3 className="text-2xl">Trending</h3>
        <CustomDropdown
          selected={trending}
          lists={trendingLists}
          handleSelect={(item) => {
            setTrending(item);
          }}
        />
      </div>
      <div className={styles.container}>
        <div className={styles.imageContainer}>
          <div className={styles.image}>
            <Image src={trendingImg} layout="fill" objectFit="contain" alt="" />
          </div>
          <div className={styles.auction}>
            <div className="flex items-center">
              <button className="mr-6 default-gradient-btn">Current Bid</button>
              <h2>
                <Algo color={!isLight ? '#fff' : '#000'} />
                {currentBid.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
              </h2>
            </div>
            <div className="flex justify-between items-center py-4 border-l border-gray-300 pl-12">
              <button className="mr-12 default-gradient-btn margin-extra">
                Ending in
              </button>
              <div className={styles.auctionWatch}>
                <h6>
                  06<span>d</span>
                </h6>{' '}
                :{' '}
                <h6>
                  03<span>h</span>
                </h6>{' '}
                :{' '}
                <h6>
                  24<span>m</span>
                </h6>{' '}
                :{' '}
                <h6>
                  09<span>s</span>
                </h6>
              </div>
            </div>
          </div>
        </div>
        <div className={styles.detail}>
          <CreatorBtn avatar={avatar} nickname={creator} />
          <h1>{title}</h1>
          <p className="dark:text-gray-200">
            Description
            <br />
            <br />
          </p>
          <p
            className="dark:text-gray-200"
            dangerouslySetInnerHTML={{ __html: description }}
          ></p>
          <div className={styles.buttons}>
            <BidBtn sizeMd={true} />
            <button className={styles.artWork}>
              <h3>VIEW ARTWORK</h3>
            </button>
          </div>
          <div className="flex mt-auto">
            <button className="default-gradient-btn btn-icon mr-3">
              <IconHeart size={20} />
            </button>
            <button className="default-gradient-btn btn-icon">
              <IconShare size={20} />
            </button>
          </div>
        </div>
      </div>
      <div className={styles.maxWidth}>
        <div className="card-box mb-8 mt-8 gap-5">
          {cards.map((card: TypeNFT, index: number) => (
            <Card item={card} key={`card-${card.tokenId}-${index}`} />
          ))}
        </div>
        <div className="flex justify-center mb-20">
          <a
            href="#"
            className={
              'text-md text-link py-8 px-5 shadow-sm rounded-21 linkBox'
            }
          >
            View all Categories
          </a>
        </div>
      </div>
    </div>
  );
};
