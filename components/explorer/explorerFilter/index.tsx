import React, { useState } from 'react';
import { FilterExpand } from '../../ui/FilterExpand';
import { TypeCurrency } from '../../utils/interface';
import styles from './ExplorerFilter.module.scss';

const currencyMap: TypeCurrency[] = [
  {
    title: 'United States Dollar (USD)',
    rate: 1,
  },
  {
    title: 'Algorand Network (ALGO)',
    rate: 100,
  },
  {
    title: 'Polygon Network (Mumbai)',
    rate: 10,
  },
];
const collections: string[] = [
  'Collection 1',
  'Collection 2',
  'Collection 3',
  'Collection 4',
  'Collection 5',
];
const chains: string[] = ['All', 'ALGORAND', 'POLYGON'];
const categories: string[] = [
  'All',
  'Digital Art',
  'Music',
  'Film',
  'Game',
  'Other',
];

export const ExplorerFilter: React.FC = () => {
  const [currentCurrency, setCurrentCurrency] = useState<number>(0);
  const [collection, setCollection] = useState<string>(collections[0]);
  const [chain, setChain] = useState<string>(chains[0]);
  const [category, setCategory] = useState<string>(categories[0]);

  return (
    <div className={`text-md py-4 ${styles.filterPage}`}>
      <div className="flex justify-between items-end pl-10 pr-8 py-8">
        <p className="text-lg">Filter</p>
        <span className="text-md text-blue-600 cursor-pointer">reset</span>
      </div>
      <FilterExpand title="Status">
        <button className="linkBox w-full py-4 mb-2 size-auto">ALL</button>
        <button className="linkBox w-full py-4 mb-2 size-auto">BUY NOW</button>
        <button className="linkBox w-full py-4 mb-2 size-auto">
          ON AUCTION
        </button>
        <button className="linkBox w-full py-4 size-auto">HAS OFFERS</button>
      </FilterExpand>
      <FilterExpand title="Price">
        <button
          className="linkBox w-full py-4 mb-4 size-auto"
          onClick={() => {
            setCurrentCurrency((currentCurrency + 1) % currencyMap.length);
          }}
        >
          {currencyMap[currentCurrency].title}
        </button>
        <div className="flex gap-3 justify-between mb-4">
          <div className="linkBox size-auto w-full p-4">
            <input
              type="text"
              className="bg-transparent border-none outline-none w-full text-center"
              placeholder="Start price"
            />
          </div>
          <div className="linkBox size-auto w-full p-4">
            <input
              type="text"
              className="bg-transparent border-none outline-none w-full text-center"
              placeholder="End price"
            />
          </div>
        </div>
        <button className="bg-btn-main btn-price text-lg font-bold">
          SET PRICE
        </button>
      </FilterExpand>
      <FilterExpand title="Collections">
        {collections.map((x: string, index: number) => (
          <button
            className="linkBox w-full py-4 mb-2 size-auto"
            key={`collection-${index}`}
            onClick={() => {
              setCollection(x);
            }}
          >
            {x}
          </button>
        ))}
      </FilterExpand>
      <FilterExpand title="Chains">
        {chains.map((x: string, index: number) => (
          <button
            className="linkBox w-full py-4 mb-2 size-auto"
            key={`chains-${index}`}
            onClick={() => {
              setChain(x);
            }}
          >
            {x}
          </button>
        ))}
      </FilterExpand>
      <FilterExpand title="Categories">
        {categories.map((x: string, index: number) => (
          <button
            className="linkBox w-full py-4 mb-2 size-auto"
            key={`category-${index}`}
            onClick={() => {
              setCategory(x);
            }}
          >
            {x}
          </button>
        ))}
      </FilterExpand>
    </div>
  );
};
