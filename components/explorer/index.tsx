import React from 'react';
import { ExplorerContent } from './explorerContent';
import { ExplorerFilter } from './explorerFilter';

export const Explorer: React.FC = () => {
  return (
    <div className="container flex px-0 dark:text-white">
      <ExplorerFilter />
      <ExplorerContent />
    </div>
  );
};
