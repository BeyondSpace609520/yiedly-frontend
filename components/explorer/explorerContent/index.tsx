import React, { useEffect, useState } from 'react';
import { Card } from '../../ui/card';
import { CustomDropdown } from '../../ui/customDropdown';
import { mockNftDatas, TypeNFT } from '../../utils/interface';
import styles from './ExplorerContent.module.scss';

const sortsList: string[] = ['Created', 'Price', 'Most Bids', 'Least Bids'];

export const ExplorerContent: React.FC = () => {
  const [sortBy, setSortBy] = useState<string>(sortsList[0]);
  const [cards, setCards] = useState<TypeNFT[]>([]);

  useEffect(() => {
    setCards(mockNftDatas);
  }, []);
  return (
    <div className={styles.contentPage}>
      <div className="py-12 px-4 flex justify-end">
        <div className="form-input size-auto w-328 bg-transparent py-4">
          <CustomDropdown
            className="w-full"
            selected={sortBy}
            lists={sortsList}
            handleSelect={(item) => {
              setSortBy(item);
            }}
          />
        </div>
      </div>
      <div className="flex gap-5 flex-wrap px-2 justify-around">
        {cards &&
          cards.map((card: TypeNFT, index: number) => (
            <Card item={card} key={`card-${index}`} />
          ))}
      </div>
      <div className="flex justify-center py-20">
        <button className="form-input px-16 text-2md size-auto py-5">
          View more
        </button>
      </div>
    </div>
  );
};
