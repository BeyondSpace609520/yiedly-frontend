import React, { useEffect, useState } from 'react';
import { Card } from '../ui/card';
import { CustomDropdown } from '../ui/customDropdown';
import { mockNftDatas, TypeNFT } from '../utils/interface';

const cateLists: string[] = ['Digital Art', 'Music', 'Film', 'Game', 'Other'];

export const Category: React.FC = () => {
  const [cate, setCate] = useState<string>(cateLists[0]);
  const [cards, setCards] = useState<TypeNFT[]>([]);

  useEffect(() => {
    setCards(mockNftDatas.slice(0, 4));
  }, []);
  return (
    <div className="container border-t border-main dark:border-opacity-30 dark:text-white">
      <div className="flex justify-between items-center my-11">
        <h3 className="text-2xl">Category</h3>
        <CustomDropdown
          selected={cate}
          lists={cateLists}
          handleSelect={(item) => {
            setCate(item);
          }}
        />
      </div>
      <div className="card-box mb-8 gap-5">
        {cards.map((card: TypeNFT, index: number) => (
          <Card item={card} key={`card-${card.tokenId}-${index}`} />
        ))}
      </div>
      <div className="flex justify-center mb-20">
        <a
          href="#"
          className={'text-md text-link py-8 px-5 shadow-sm rounded-21 linkBox'}
        >
          View all Categories
        </a>
      </div>
    </div>
  );
};
