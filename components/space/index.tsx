import React, { useState } from 'react';
import { CollectionCard, CollectionData } from '../ui/collectionCard';
import { CustomDropdown } from '../ui/customDropdown';

const collectionCards: CollectionData[] = [
  {
    img: '/img/collection/5.png',
    collection: 'Vortextes',
    creatorName: 'Sara Oudania',
  },
  {
    img: '/img/collection/1.png',
    collection: 'Skins',
    creatorName: 'Stitchhbob',
  },
  {
    img: '/img/collection/2.png',
    collection: 'Dimensions',
    creatorName: 'Stitchhbob',
  },
  {
    img: '/img/collection/3.png',
    collection: 'TEaMS',
    creatorName: 'Stitchhbob',
  },
  {
    img: '/img/collection/4.png',
    collection: 'Assets',
    creatorName: 'Stitchhbob',
  },
];

const collectionLists: string[] = ['Popular', 'Music', 'Film', 'Game', 'Other'];

export const Space: React.FC = () => {
  const [collection, setCollection] = useState<string>(collectionLists[0]);
  return (
    <div className="container border-t border-main dark:border-opacity-30 dark:text-white">
      <div className="flex justify-between items-center my-11">
        <h3 className="text-2xl">Space</h3>
        <CustomDropdown
          selected={collection}
          lists={collectionLists}
          handleSelect={(item) => {
            setCollection(item);
          }}
        />
      </div>
      <div className="mb-12 gap-6 flex flex-col lg:flex-row">
        <div className="gap-6 flex flex-wrap space-left">
          <CollectionCard item={collectionCards[1]} />
          <CollectionCard item={collectionCards[2]} />
          <CollectionCard item={collectionCards[3]} />
          <CollectionCard item={collectionCards[4]} />
        </div>
        <CollectionCard item={collectionCards[0]} sizeMd />
      </div>
      <div className="flex justify-center mb-20">
        <a
          href="#"
          className={'text-md text-link py-8 px-5 shadow-sm rounded-21 linkBox'}
        >
          View all Spaces
        </a>
      </div>
    </div>
  );
};
