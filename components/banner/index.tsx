import React, { useContext, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import styles from './Banner.module.scss';
import { Algo } from '../ui/Icons';
import { BidBtn } from '../ui/bidBtn';
import { CreatorBtn } from '../ui/creatorBtn';
import { ThemeContext } from '../../theme/ThemeProvider';
type Props = {
  isLight?: boolean;
  featuredImg?: string;
  title?: string;
  currentBid?: number;
};

export const Banner: React.FC<Props> = ({
  isLight,
  featuredImg = '/img/Lion_01.png',
  title = 'VALOUR',
  currentBid = 5636,
}: Props) => {
  const { theme, toggleTheme } = useContext(ThemeContext);
  return (
    <>
      <div className={styles.container}>
        <div className={styles.imageContainer}>
          <div className={styles.image}>
            <Image src={featuredImg} layout="fill" objectFit="contain" alt="" />
          </div>
        </div>
        <div className={`dark:text-white ${styles.detail}`}>
          <CreatorBtn avatar="/img/avatar.jpg" nickname="MaD Lions" />
          <div className={styles.title}>{title}</div>
          <div
            className={`${styles.auction} border-t border-b border-main dark:border-opacity-30`}
          >
            <div className="flex items-center justify-between mb-5">
              <p>Drops in</p>
              <div className={`dark:bg-progress ${styles.progressContainer}`}>
                <div
                  className={styles.progressIndicator}
                  style={{ width: '30%' }}
                ></div>
              </div>
            </div>
            <div className={styles.auctionWatch}>06d : 03h : 24m : 09s</div>
          </div>
          <p>Current Bid</p>
          <div className="flex items-center">
            <h2>
              <Algo color={theme === 'dark' ? '#fff' : '#000'} />
              {currentBid.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            </h2>
            <h3>
              $
              {Math.round(currentBid * 1.95)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            </h3>
          </div>
          <div className={styles.buttons}>
            <BidBtn sizeMd={true} />
            <button className={styles.artWork}>
              <h3>VIEW ARTWORK</h3>
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
