import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './Hamburger.module.scss';
import { useCallback, useEffect, useState } from 'react';
import { IconDark, IconWallet } from '../ui/Icons';

declare type PageProps = {
  isLight: boolean;
  handleChangeMode: () => void;
};

export const Hamburger = ({ isLight, handleChangeMode }: PageProps) => {
  const [hamOpen, setHamOpen] = useState(false);
  const router = useRouter();
  const toCurrentRouteClass = (expectedRoute: string) =>
    router.asPath === expectedRoute ? styles.currentRoute : '';

  useEffect(() => {
    if (hamOpen) {
      document.body.className += ' ' + 'lock-scroll';
    } else {
      document.body.className = '';
    }
  }, [hamOpen]);

  const setHam = useCallback(
    (route: string, forceClose: boolean = false) => {
      if (router.asPath === route || forceClose) {
        setHamOpen(false);
      }
    },
    [router.asPath],
  );

  return (
    <>
      <div className={styles.overlay} aria-expanded={hamOpen} />
      <div className={styles.container} aria-expanded={hamOpen}>
        <div className={styles.navItemList}>
          <div className={toCurrentRouteClass('/')} onClick={() => setHam('/')}>
            <Link href="/" passHref>
              <a>Home</a>
            </Link>
          </div>
          <div
            className={toCurrentRouteClass('/explorer')}
            onClick={() => setHam('/explorer', true)}
          >
            <Link href="/explorer" passHref>
              <a>BROWSE MARKETPLACE</a>
            </Link>
          </div>
          <div
            className={toCurrentRouteClass('/create')}
            onClick={() => setHam('/create', true)}
          >
            <Link href="/create" passHref>
              <a>CREATE NFTs</a>
            </Link>
          </div>
          <div className={styles.navBtns}>
            <div
              className={styles.linkBox}
              onClick={() => {
                handleChangeMode();
                setHamOpen(false);
              }}
            >
              <IconDark color={isLight ? '#7200FF' : '#AC70F6'} />
            </div>
            <div
              className={styles.linkBox}
              onClick={() => {
                setHamOpen(false);
              }}
            >
              <IconWallet color={isLight ? '#7200FF' : '#AC70F6'} />
            </div>
          </div>
        </div>
      </div>
      <div
        className={styles.navIcon}
        aria-expanded={hamOpen}
        onClick={() => setHamOpen(!hamOpen)}
      >
        <span></span>
        <span></span>
        <span></span>
      </div>
    </>
  );
};
