import { useRouter } from 'next/router';
import { useEffect, useState, useContext } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import Web3 from 'web3';
import axios from 'axios';
import { toast } from 'react-toastify';
import { loadStdlib } from '@reach-sh/stdlib';
import MyAlgoConnect from '@reach-sh/stdlib/ALGO_MyAlgoConnect';

import styles from './Header.module.scss';

import { ThemeContext } from '../../theme/ThemeProvider';
import { Hamburger } from './Hamburger';
import { IconDark, IconWallet, IconLight } from '../ui/Icons';
import { WrongNetwork } from '../ui/wrongNetwork';
import { shortenAddress } from '../utils/web3Functions';
import { CHAIN_ID, CRYPTO_TYPE } from '../../constant';
import ConnectWalletModal from '../ui/connectWalletModal';

// import * as algoService from '../../services/algoService.mjs';

const stdlib = loadStdlib('ALGO');
stdlib.setWalletFallback(
  stdlib.walletFallback({
    providerEnv: 'TestNet',
    MyAlgoConnect,
  }),
);

// const getAccountInfo = async (addr) => {
//   return (
//     await axios.get(`https://testnet.algoexplorerapi.io/v2/accounts/${addr}`)
//   )?.data;
// };

const Header: React.FC = () => {
  const router = useRouter();
  const { theme, toggleTheme } = useContext(ThemeContext);

  const [searchText, setSearchText] = useState<string>('');
  const [isLight, setIsLight] = useState<boolean>(true);
  const [walletAddress, setWalletAddress] = useState<string>('');
  const [balance, setBalance] = useState<string>('');
  const [isConnected, setIsConnected] = useState<boolean>(false);
  const [chainId, setChainId] = useState<number>(CHAIN_ID);
  const [isOpenConnectModal, setIsOpenConnectModal] = useState<boolean>(false);
  const [cryptoType, setCryptoType] = useState<string>('');

  const isNotLoginRoute = router.asPath !== '/login';

  useEffect(() => {
    getProvider();
  }, []);

  useEffect(() => {
    const win = window as any;
    if (win.ethereum) {
      win.ethereum.on('chainChanged', updateChainId);
      win.ethereum.on('accountsChanged', updateWalletAddress);
    }

    return () => {
      win?.ethereum.removeListener('chainChanged', updateChainId);
      win?.ethereum.removeListener('accountsChanged', updateWalletAddress);
    };
  }, []);

  useEffect(() => {
    console.log(chainId);
    if (chainId === CHAIN_ID) {
      if (walletAddress) setIsConnected(true);
      document.body.style.overflowY = 'auto';
    } else {
      setIsConnected(false);
      document.body.style.overflowY = 'hidden';
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [chainId]);

  const toCurrentRouteClass = (expectedRoute: string) =>
    router.asPath === expectedRoute ? styles.currentRoute : '';

  const toggleConnectModal = () => setIsOpenConnectModal(!isOpenConnectModal);

  const updateChainId = (newChainId: string) => {
    setChainId(parseInt(newChainId));
  };

  const updateWalletAddress = (accounts: string[]) => {
    console.log(accounts);
    if (accounts.length > 0) {
      setWalletAddress(accounts[0]);
    } else {
      setWalletAddress('');
      setIsConnected(false);
    }
  };

  const getProvider = async () => {
    if (typeof window !== 'undefined') {
      if (window.hasOwnProperty('web3')) {
        const wa = window as any;
        wa.web3 = new Web3(wa.web3.currentProvider);
        if (wa.web3) {
          const accounts = await wa.web3.eth.getAccounts();
          console.log(accounts);
          if (accounts.length > 0) {
            setWalletAddress(accounts[0]);
            setIsConnected(true);
          } else {
            setWalletAddress('');
            setIsConnected(false);
          }
          const newChainId = await wa.web3.eth.getChainId();
          setChainId(newChainId);
        }
        // if ((await wa.web3.eth.getChainId()) !== CHAIN_ID) {
        //   setIsConnected(false);
        //   toast.error(CHAIN_ID_ERR_MSG);
        // }
      }
    }
  };

  if (cryptoType === CRYPTO_TYPE.ETH && walletAddress) {
    const wa = window as any;
    wa.web3.eth.getBalance(walletAddress).then((balance: any) => {
      setBalance(
        parseFloat(wa.web3.utils.fromWei(balance, 'ether')).toFixed(2),
      );
    });
  }

  const connectMetamasks = async () => {
    const wa = window as any;
    if (window.hasOwnProperty('ethereum')) {
      wa.web3 = new Web3(wa.ethereum);
      await wa.ethereum.enable();
    } else if (window.hasOwnProperty('web3')) {
      wa.web3 = new Web3(wa.web3.currentProvider);
    } else {
      toast.error(
        'Non-Ethereum browser detected. You should consider trying MetaMask!',
      );
      return;
    }

    const accounts = await wa.web3.eth.getAccounts();
    setWalletAddress(accounts[0]);
    const balance = await wa.web3.eth.getBalance(accounts[0]);
    setBalance(parseFloat(wa.web3.utils.fromWei(balance, 'ether')).toFixed(2));

    toggleConnectModal();
    setIsConnected(true);
  };

  const connectMyAlgo = async () => {
    let acc = null;
    try {
      acc = await stdlib.getDefaultAccount();
      const balAtomic = await stdlib.balanceOf(acc);
      const balanace = stdlib.formatCurrency(balAtomic, 4);

      setWalletAddress(acc.networkAccount.addr);
      setBalance(balanace);

      toggleConnectModal();
      setIsConnected(true);
    } catch (e) {
      console.log(e);
    }
    return acc;
  };

  return (
    <div className="dark:bg-card">
      <ConnectWalletModal
        open={isOpenConnectModal}
        handleMetaMaskConnect={connectMetamasks}
        handleMyAlgoConnect={connectMyAlgo}
        setCryptoType={setCryptoType}
      />

      {/* {chainId !== CHAIN_ID && <WrongNetwork />} */}

      {isNotLoginRoute && (
        <Hamburger
          isLight={isLight}
          handleChangeMode={() => {
            setIsLight(!isLight);
          }}
        />
      )}

      <div className={`dark:bg-card ${styles.container}`}>
        <div className={styles.headerLeft}>
          <Link href="/" passHref>
            <div className={styles.logo}>
              <Image
                src="/img/logo.png"
                layout="fill"
                objectFit="contain"
                alt=""
              />
            </div>
          </Link>
          <div className="search_container">
            <input
              type="text"
              className={`dark:text-white ${styles.search}`}
              placeholder="Search Marketplace"
              value={searchText}
              onChange={(e) => {
                setSearchText(e.target.value);
              }}
            />
          </div>
        </div>

        {isNotLoginRoute && (
          <div className={styles.linkContainer}>
            <div className="linkBox">
              <div className={toCurrentRouteClass('/explorer')}>
                <Link href="/explorer">BROWSE MARKETPLACE</Link>
              </div>
              <div className={`${toCurrentRouteClass('/create')}`}>
                <Link href="/create">CREATE NFTs</Link>
              </div>
            </div>
            <div
              className="linkBox"
              onClick={() => {
                setIsLight(!isLight);
                toggleTheme();
              }}
            >
              {theme === 'dark' ? (
                <IconLight color={!isLight ? '#7200FF' : '#AC70F6'} />
              ) : (
                <IconDark color={!isLight ? '#7200FF' : '#AC70F6'} />
              )}
            </div>
            {!isConnected ? (
              <div className="linkBox" onClick={toggleConnectModal}>
                <IconWallet color={!isLight ? '#7200FF' : '#AC70F6'} />
              </div>
            ) : (
              <div className="flex flex-col items-center mx-4 dark:text-white">
                <p>{shortenAddress(walletAddress)}</p>
                {balance} {cryptoType === CRYPTO_TYPE.ETH ? 'MATIC' : 'ALGO'}
              </div>
            )}
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
