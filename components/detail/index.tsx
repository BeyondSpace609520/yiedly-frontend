import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import Image from 'next/image';
import { TypeNFT, TypeNFTHistory } from '../utils/interface';
import styles from './Detail.module.scss';
import { DetailUser } from './detailUser';
import { Nickname } from '../ui/card/nickname';
import {
  IconExpand,
  IconIPFS,
  IconMetadata,
  IconMore,
  IconPoly,
  IconShare,
} from '../ui/Icons';
import { BidBtn } from '../ui/bidBtn';
import { BidderBox } from '../ui/BidderBox';

const mockNftData: TypeNFT = {
  tokenId: '1',
  auctionInfo: null,
  category: 'art',
  creator: {
    id: '1',
    addressPoly: [
      '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
    ],
    addressAlgo: ['OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4'],
    avatarImage: '/img/avatar.jpg',
    nickName: '@nickName',
    userName: 'User Name',
    bio: 'This is test user',
    followerCount: 4,
    followingCount: 2,
  },
  owner: {
    id: '1',
    addressPoly: [
      '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
    ],
    addressAlgo: ['OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4'],
    avatarImage: '/img/avatar.jpg',
    nickName: '@nickName',
    userName: 'User Name',
    bio: 'This is test user',
    followerCount: 4,
    followingCount: 2,
  },
  currencyType: 'ALGO',
  title: 'Test NFT',
  description: 'This is test NFT',
  image: '/img/nft/1.png',
  imageType: 'image',
  isSale: true,
  price: 5636,
  tokenURI:
    'https://ipfs.io/ipfs/bafybeic6zypfb72zbk3guhrhs3jhywrlhhxvcrclh2qwhcw2rdh7dfn6q4/metadata.json',
  created: 1629971030309,
  followingCount: 0,
  bidHistory: {},
  totalHistory: {},
  tags: ['3d makeup', 'digital makeup', 'digital', 'cosmic', 'new dimensions'],
};

const mockBidHistories: TypeNFTHistory[] = [
  {
    tokenId: '1',
    walletAddress:
      '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
    walletType: 'POLY',
    user: {
      id: '1',
      addressPoly: [
        '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
      ],
      addressAlgo: [
        'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
      ],
      avatarImage: '/img/avatar.jpg',
      nickName: '@nickName',
      userName: 'User Name',
      bio: 'This is test user',
      followerCount: 4,
      followingCount: 2,
    },
    time: 1636104675193,
    actionType: 'bid',
    price: 15,
    priceUSD: 66000,
    transactionURL: 'transaction history',
  },
  {
    tokenId: '1',
    walletAddress:
      '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
    walletType: 'POLY',
    user: {
      id: '1',
      addressPoly: [
        '0xOOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
      ],
      addressAlgo: [
        'OOEKZRB4DN4GEJSXDBOCCWQAZYJQG7Z4VPYHZQNN7E6YIFXHBFHTCFU5Y4',
      ],
      avatarImage: '/img/avatar.jpg',
      nickName: '@nickName',
      userName: 'User Name',
      bio: 'This is test user',
      followerCount: 4,
      followingCount: 2,
    },
    time: 1636104675193,
    actionType: 'bid',
    price: 15,
    priceUSD: 66000,
    transactionURL: 'transaction history',
  },
];

export const Detail: React.FC = () => {
  const router = useRouter();
  const { tokenId } = router.query;
  console.log(tokenId);
  const [nftData, setNftData] = useState<TypeNFT | null>(null);
  const [putOnSale, setPutOnSale] = useState<boolean>(false);
  const [rate, setRate] = useState<number>(1.5224);

  useEffect(() => {
    const getNftData = () => {
      setNftData(mockNftData);
      setPutOnSale(mockNftData.isSale);
    };

    getNftData();
  }, []);
  return (
    <div className="dark:text-white">
      {nftData ? (
        <>
          <div className={`dark:bg-black ${styles.imgBox}`}>
            <div className={styles.detailImg}>
              <Image
                src={nftData.image}
                layout="fill"
                objectFit="contain"
                alt=""
              />
            </div>
          </div>
          <div className="container dark:bg-body">
            <div className="flex justify-between items-center py-8">
              <Nickname
                avatar={nftData.owner.avatarImage}
                nickname={nftData.owner.nickName}
              />
              <div className="flex items-center">
                <button className="linkBox angle-100 px-5 rounded-full mr-6">
                  <IconMore />
                </button>
                <button className="linkBox angle-100 px-7 rounded-full">
                  <IconShare size={24} />
                </button>
              </div>
            </div>
            <div className="flex flex-col lg:flex-row">
              <div className="w-full mb-16">
                <div className="mb-20">
                  <h3 className="text-3xl mb-12">{mockNftData.title}</h3>
                  <p className="text-base mb-4">Description</p>
                  <p
                    className="text-base mb-10"
                    dangerouslySetInnerHTML={{
                      __html: mockNftData.description,
                    }}
                  ></p>
                  <Link href={mockNftData.owner.id} passHref>
                    <span className="border-b border-main">
                      About the Artist
                    </span>
                  </Link>
                </div>
                <div className="flex flex-col md:flex-row mr-20">
                  <div className="mr-40 mb-16">
                    <p className="text-base mb-12">Edition of</p>
                    <div className="linkBox w-28 size-auto text-3xl p-2 px-4">
                      01
                    </div>
                  </div>
                  <div className="mb-16">
                    <p className="text-base mb-12">Tags</p>
                    <div className="flex flex-wrap gap-3">
                      {mockNftData.tags.map((tag) => (
                        <span
                          key={tag}
                          className="bg-btn-main text-sm size-auto px-5"
                        >
                          {tag}
                        </span>
                      ))}
                    </div>
                  </div>
                </div>
                <div>
                  <div className="linkBox size-auto w-328 flex justify-between items-center mb-3 px-5 py-3">
                    <IconPoly />
                    <p className="text-base ml-5 mr-auto">View in Polyscan</p>
                    <Link href="/">
                      <a target="_blank">
                        <IconExpand />
                      </a>
                    </Link>
                  </div>
                  <div className="linkBox size-auto w-328 flex justify-between items-center mb-3 px-5 py-3">
                    <IconIPFS />
                    <p className="text-base ml-5 mr-auto">View in IPFS</p>
                    <Link href="/">
                      <a target="_blank">
                        <IconExpand />
                      </a>
                    </Link>
                  </div>
                  <div className="linkBox size-auto w-328 flex justify-between items-center mb-3 px-5 py-3">
                    <IconMetadata />
                    <p className="text-base ml-5 mr-auto">View in Metadata</p>
                    <Link href="/">
                      <a target="_blank">
                        <IconExpand />
                      </a>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="w-450 flex-shrink-0 mb-16">
                <div className="border-t border-gray-500 dark:border-gray-300 pt-3">
                  <p className="text-base mb-12">Drops in</p>
                  <div
                    className={`dark:bg-progress mb-8 ${styles.progressContainer}`}
                  >
                    <div
                      className={styles.progressIndicator}
                      style={{ width: '30%' }}
                    ></div>
                  </div>
                  <div className={`flex mb-12 ${styles.auctionWatch}`}>
                    <h6>
                      06<span>d</span>
                    </h6>{' '}
                    :{' '}
                    <h6>
                      03<span>h</span>
                    </h6>{' '}
                    :{' '}
                    <h6>
                      24<span>m</span>
                    </h6>{' '}
                    :{' '}
                    <h6>
                      09<span>s</span>
                    </h6>
                  </div>
                </div>
                <div className="border-t border-gray-500 dark:border-gray-300 pt-3 mb-28">
                  <p className="text-base mb-12">Current Bid</p>
                  <div className="flex text-2xl items-center mb-8">
                    <div className="flex items-center mr-8">
                      <Image
                        src={`/img/coin/${mockNftData.currencyType}.svg`}
                        width="32px"
                        height="32px"
                        alt=""
                      />
                      <p className="ml-4">
                        {mockNftData.price
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                      </p>
                    </div>
                    <p className="text-gray-500">
                      $
                      {(mockNftData.price * rate)
                        .toFixed(2)
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    </p>
                  </div>
                  <div>
                    <BidBtn className="w-full py-3" />
                  </div>
                </div>
                <div className="mb-16">
                  <p className="text-xl mb-12">Provenance</p>
                  <div className="flex gap-5 overflow-x-auto">
                    {mockBidHistories.map(
                      (bidHistory: TypeNFTHistory, index: number) => (
                        <BidderBox
                          bidHistory={bidHistory}
                          key={`bidHistory-${index}`}
                        />
                      ),
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="flex flex-col justify-between lg:flex-row py-20 mb-12 border-t border-create">
              <div className="flex items-center mb-12 lg:mb-0">
                <div className={styles.avatarImage}>
                  <Image
                    src={mockNftData.owner.avatarImage}
                    layout="fill"
                    objectFit="contain"
                    alt=""
                  />
                </div>
                <div className={styles.ownerName}>
                  <h2>{mockNftData.owner.userName}</h2>
                  <p className="text-xl xl:text-2xl text-purple">
                    {mockNftData.owner.nickName}
                  </p>
                </div>
              </div>
              <p
                className={`text-md xl:text-lg text-gray-300 ${styles.userDescription}`}
              >
                FACT are one of the more stable and professional esports clubs
                in Norway - A growing esports nation with an already
                well-established esports culture. Fielding top athletes and
                teams in our respective esports campaigns. Ever since our
                inception we have raised the bar and set new standards in the
                space.
              </p>
            </div>
          </div>
        </>
      ) : (
        <div className="flex justify-center items-center h-full text-2xl">
          No NFT Data
        </div>
      )}
    </div>
  );
};
