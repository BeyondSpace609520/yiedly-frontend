import React from 'react';
import Image from 'next/image';
import { TypeNFTOwner } from '../../utils/interface';
import { Nickname } from '../../ui/card/nickname';

declare type PageProps = {
  user: TypeNFTOwner;
};

export const DetailUser = ({ user }: PageProps) => {
  return (
    <div className="flex items-center items-center dark:text-white">
      <div className="relative flex items-center mr-3 rounded-full border border-gray-500">
        <Image
          src={user.avatarImage}
          width="44px"
          className="rounded-full"
          height="44px"
          alt=""
        />
      </div>
      <div className="flex flex-col">
        <Nickname avatar={user.avatarImage} nickname={user.nickName} />
      </div>
    </div>
  );
};
