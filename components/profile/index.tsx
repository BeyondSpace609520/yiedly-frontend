import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { Card } from '../ui/card';
import {
  IconCopy,
  IconVerifyBadge,
  IconInstagram,
  Icontwitter,
  IconOther,
} from '../ui/Icons';
import { shortenAddress } from '../utils/web3Functions';
import { Banner } from './banner';
import { mockNftDatas, TypeNFT } from '../utils/interface';

export type ProfileProps = {
  collectAvatars?: string[];
  name?: string;
  userName?: string;
  userId?: string;
  walletAddress?: string;
  followersCount?: number;
  followingCount?: number;
  twitterAccount?: string;
  instagramAccount?: string;
  invitedBy?: string;
  bio?: string;
  joinDate?: string;
};

const Profile: React.FC<ProfileProps> = ({
  collectAvatars = [
    '/img/avatar/profile_avatar1.jpg',
    '/img/avatar/profile_avatar2.jpg',
    '/img/avatar/profile_avatar3.jpg',
    '/img/avatar/profile_avatar4.jpg',
    '/img/avatar/profile_avatar5.jpg',
  ],
  name = 'MAD Lions',
  userName = '@MADLions',
  userId = '#7645',
  walletAddress = '0x4128ECF5056DC9D0a84Dad796b1AB27BeD32134d',
  followersCount = 3406,
  followingCount = 42,
  twitterAccount = 'MADLions',
  instagramAccount = 'madlions_en',
  invitedBy = 'Boomesports',
  bio = 'MAD Lions is a Spanish League of Legends team and back-to-back winner of the LEC. Fueled by a passion for life & gaming.',
  joinDate = 'April 2021',
}: ProfileProps) => {
  const collectAvatar = [];
  const [cards, setCards] = useState<TypeNFT[]>([]);

  useEffect(() => {
    setCards(mockNftDatas);
  }, []);
  for (let i = 1; i <= 3; i++) {
    collectAvatar.push(
      <div className="m-0" key={`collect-${i}`}>
        <div className="collect-btn-avatar-container" aria-expanded="false">
          <Link href="/" passHref>
            <div
              className="collect-btn-avatar"
              style={{ backgroundImage: `url(${collectAvatars[i]})` }}
              role="image"
            ></div>
          </Link>
        </div>
      </div>,
    );
  }
  return (
    <>
      <Banner />
      <div className="profile-content">
        <div className="m-0">
          <div className="profile-wallet-outbox">
            <div className="profile-wallet-container">
              <div className="profile-wallet">
                <div className="profile-wallet-usernum">{userId}</div>
                <div className="profile-wallet-address dark:text-white">
                  {shortenAddress(walletAddress)}
                </div>
                <div
                  className="profile-wallet-copy"
                  aria-label="Copy Address"
                  data-balloon-pos="up"
                >
                  <IconCopy />
                </div>
              </div>
            </div>
          </div>
          <div className="box-sizing-box">
            <div className="profile-grid">
              <div className="my-3 flex flex-col items-center md:items-start">
                <div className="profile-name-container">
                  <div className="m-0">
                    <div className="profile-name dark:text-white">{name}</div>
                  </div>
                </div>
                <div className="profile-username-container">
                  <div className="profile-username">{userName}</div>
                </div>
              </div>
              <div className="profile-grid-wide">
                <div className="profile-grid">
                  <div className="flex justify-center">
                    <div className="collect-btn-container-shift">
                      <div className="collect-btn">
                        <div className="mr-2 font-semibold dark:text-white">
                          Collected by
                        </div>
                        <div className="position-relative z-index-2">
                          <div className="flex">{collectAvatar}</div>
                        </div>
                        <div className="display collect-btn-viewall text-gray-50 text-opacity-30 dark:hover:text-white ml-4">
                          View all
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="profile-follow">
                    <div className="profile-follow-following">
                      <div className="profile-follow-following-inside">
                        <div className="profile-font-18">{followingCount}</div>
                        <div className="profile-font-16">Following</div>
                      </div>
                    </div>
                    <div className="profile-follow-following">
                      <div className="profile-follow-following-inside">
                        <div className="profile-font-18">{followersCount}</div>
                        <div className="profile-font-16">Followers</div>
                      </div>
                    </div>
                    <div className="profile-btn-wrap">
                      <button className="profile-follow-btn">Follow</button>
                    </div>
                  </div>
                  <div className="box-sizing-box">
                    <h2 className="profile-font-18">Followed by</h2>
                    <div className="profile-followers">
                      <div className="profile-btn-wrap">
                        {collectAvatars.map((item, index) => (
                          <div
                            className="box-sizing-box"
                            key={`profile-avatars-${index}`}
                          >
                            <div
                              className="collect-btn-avatar-container"
                              aria-expanded="false"
                            >
                              <Link href="/@PooFeeK" passHref>
                                <div
                                  className="collect-btn-avatar"
                                  style={{ backgroundImage: `url(${item})` }}
                                  role="image"
                                ></div>
                              </Link>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                    <div className="profile-btn-wrap">
                      <div className="profile-follow-viewall">View all</div>
                    </div>
                  </div>
                </div>
                <div className="profile-display">
                  <div className="profile-grid-wide">
                    <div className="profile-out-net">
                      <div className="profile-btn-wrap">
                        <div className="profile-transition">
                          <Link
                            href={`https://twitter.com/${twitterAccount}`}
                            passHref
                          >
                            <a className="profile-twitter out-net">
                              <IconVerifyBadge />
                              <div className="profile-font-14">{`@${twitterAccount}`}</div>
                              <Icontwitter color={'#000'} />
                            </a>
                          </Link>
                        </div>
                      </div>
                      <div className="profile-btn-wrap">
                        <div className="profile-transition">
                          <Link
                            href={`https://instagram.com/${instagramAccount}`}
                            passHref
                          >
                            <a className="profile-twitter out-net">
                              <IconVerifyBadge />
                              <div className="profile-font-14">{`@${instagramAccount}`}</div>
                              <IconInstagram color={'#000'} />
                            </a>
                          </Link>
                        </div>
                      </div>
                    </div>
                    <div className="box-sizing-box">
                      <div className="profile-bio dark:text-white">Bio</div>
                      <div className="box-sizing-box">
                        <div className="profile-bio-content">
                          <p className="dark:text-white">{bio}</p>
                        </div>
                      </div>
                    </div>
                    <div className="profile-joined">
                      <div className="profile-join-font  dark:text-white">
                        Joined
                      </div>
                      <div className="profile-join-date  dark:text-white">
                        {joinDate}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="m-0">
          <div className="border-b border-white border-solid pb-2 mb-4">
            <div className="text-black dark:text-white font-semibold text-lg mb-2 cursor-pointer">
              Collections
            </div>
          </div>
          <div className="profile-display-grid">
            <div className="profile-display-grid-inside">
              {cards.map((card: TypeNFT, index: number) => (
                <Card item={card} key={`card-${card.tokenId}-${index}`} />
              ))}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Profile;
