import React from 'react';
import Link from 'next/link';
export type BannerProps = {
  banner?: string;
  avatar?: string;
  collectAvatars?: Array<string>;
};

export const Banner: React.FC<BannerProps> = ({
  banner = '/img/avatar/profile_banner.jpg',
  avatar = '/img/avatar/profile_avatar.jpg',
  collectAvatars = [
    '/img/avatar/profile_avatar1.jpg',
    '/img/avatar/profile_avatar2.jpg',
    '/img/avatar/profile_avatar3.jpg',
    '/img/avatar/profile_avatar4.jpg',
    '/img/avatar/profile_avatar5.jpg',
  ],
}: BannerProps) => {
  const collectAvatar = [];
  for (let i = 1; i <= 3; i++) {
    collectAvatar.push(
      <div className="m-0" key={`collect-${i}`}>
        <div className="collect-btn-avatar-container" aria-expanded="false">
          <Link href="/" passHref>
            <div
              className="collect-btn-avatar"
              style={{ backgroundImage: `url(${collectAvatars[i]})` }}
              role="image"
            ></div>
          </Link>
        </div>
      </div>,
    );
  }
  return (
    <div className="box-border sm:mb-36 mb-20 relative">
      <div className="box-border m-0 relative">
        <div className="profile-banner-bg-gradient"></div>
        <div
          className="profile-banner-image"
          style={{ backgroundImage: `url(${banner})` }}
        ></div>
      </div>
      <div className="profile-avatar-bar">
        <div className="profile-avatar-container">
          <div
            className="profile-avatar"
            style={{ backgroundImage: `url(${avatar})` }}
          ></div>
        </div>
        <div className="collect-btn-container">
          <div className="collect-btn">
            <div className="mr-2 font-lg font-semibold dark:text-white">
              Collected by
            </div>
            <div className="position-relative z-index-2">
              <div className="flex">{collectAvatar}</div>
            </div>
            <div className="display  text-gray-400 dark:hover:text-white ml-4">
              <Link href="/" passHref>
                View all
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
