// Automatically generated with Reach 0.1.6
/* eslint-disable */
export const _version = '0.1.6';
export const _backendVersion = 5;

export function getExports(s) {
  const stdlib = s.reachStdlib;
  return {};
}
export function _getViews(s, viewlib) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Address;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_UInt;

  return {
    infos: {
      Auction: {
        currentPrice: {
          decode: async (i, svs, args) => {
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1),
              )
            ) {
              const [v196, v197, v198] = svs;
              stdlib.assert(false, 'illegal view');
            }
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
              )
            ) {
              const [v196, v197, v210, v211, v212, v217, v221, v222] = svs;
              return await (async () => {
                return v210;
              })(...args);
            }

            stdlib.assert(false, 'illegal view');
          },
          ty: ctc2,
        },
        endSecs: {
          decode: async (i, svs, args) => {
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1),
              )
            ) {
              const [v196, v197, v198] = svs;
              stdlib.assert(false, 'illegal view');
            }
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
              )
            ) {
              const [v196, v197, v210, v211, v212, v217, v221, v222] = svs;
              return await (async () => {
                return v211;
              })(...args);
            }

            stdlib.assert(false, 'illegal view');
          },
          ty: ctc2,
        },
        highestBidder: {
          decode: async (i, svs, args) => {
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1),
              )
            ) {
              const [v196, v197, v198] = svs;
              stdlib.assert(false, 'illegal view');
            }
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
              )
            ) {
              const [v196, v197, v210, v211, v212, v217, v221, v222] = svs;
              return await (async () => {
                return v212;
              })(...args);
            }

            stdlib.assert(false, 'illegal view');
          },
          ty: ctc0,
        },
        nft: {
          decode: async (i, svs, args) => {
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 1),
              )
            ) {
              const [v196, v197, v198] = svs;
              stdlib.assert(false, 'illegal view');
            }
            if (
              stdlib.eq(
                i,
                stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
              )
            ) {
              const [v196, v197, v210, v211, v212, v217, v221, v222] = svs;
              return await (async () => {
                return v197;
              })(...args);
            }

            stdlib.assert(false, 'illegal view');
          },
          ty: ctc1,
        },
      },
    },
    views: {
      1: [ctc0, ctc1, ctc2],
      4: [ctc0, ctc1, ctc2, ctc2, ctc0, ctc2, ctc2, ctc2],
    },
  };
}
export function _getMaps(s) {
  const stdlib = s.reachStdlib;
  const ctc0 = stdlib.T_Tuple([]);
  return {
    mapDataTy: ctc0,
  };
}
export async function Bid_bid(ctcTop, interact) {
  if (typeof ctcTop !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(
      new Error(
        `The backend for Bid_bid expects to receive a contract as its first argument.`,
      ),
    );
  }
  if (typeof interact !== 'object') {
    return Promise.reject(
      new Error(
        `The backend for Bid_bid expects to receive an interact object as its second argument.`,
      ),
    );
  }
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Address;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_UInt;
  const ctc3 = stdlib.T_Tuple([ctc2]);
  const ctc4 = stdlib.T_Data({
    Bid_bid0: ctc3,
    Bid_close0: ctc3,
  });
  const ctc5 = stdlib.T_Null;

  const [v196, v197, v210, v211, v212, v217, v221, v222] = await ctc.getState(
    stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
    [ctc0, ctc1, ctc2, ctc2, ctc0, ctc2, ctc2, ctc2],
  );
  const v235 = stdlib.protect(ctc3, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: [
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
      'at ./index.rsh:53:19:application call to "runBid_bid0" (defined at: ./index.rsh:53:19:function exp)',
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:53:19:function exp)',
    ],
    msg: 'in',
    who: 'Bid_bid',
  });
  const v236 =
    v235[
      stdlib.checkedBigNumberify('./index.rsh:76:11:spread', stdlib.UInt_max, 0)
    ];
  const v237 = stdlib.gt(v236, v210);
  stdlib.assert(v237, {
    at: './index.rsh:77:25:application',
    fs: [
      'at ./index.rsh:76:11:application call to [unknown function] (defined at: ./index.rsh:77:16:function exp)',
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
      'at ./index.rsh:53:19:application call to "runBid_bid0" (defined at: ./index.rsh:53:19:function exp)',
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:53:19:function exp)',
    ],
    msg: null,
    who: 'Bid_bid',
  });

  const v241 = ['Bid_bid0', v235];

  const txn1 = await ctc.sendrecv({
    args: [v196, v197, v210, v211, v212, v217, v221, v222, v241],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc4],
    pay: [v236, []],
    sim_p: async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };

      const {
        data: [v246],
        secs: v248,
        time: v247,
        didSend: v133,
        from: v245,
      } = txn1;

      switch (v246[0]) {
        case 'Bid_bid0': {
          const v249 = v246[1];
          const v250 =
            v249[
              stdlib.checkedBigNumberify(
                './index.rsh:76:11:spread',
                stdlib.UInt_max,
                0,
              )
            ];
          const v253 = stdlib.add(v221, v250);
          sim_r.txns.push({
            amt: v250,
            kind: 'to',
            tok: undefined,
          });
          const v256 = stdlib.gt(v250, v210);
          stdlib.assert(v256, {
            at: './index.rsh:80:18:application',
            fs: [
              'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
            ],
            msg: null,
            who: 'Bid_bid',
          });
          const v260 = stdlib.sub(v253, v210);
          sim_r.txns.push({
            amt: v210,
            kind: 'from',
            to: v212,
            tok: undefined,
          });
          const v261 = stdlib.checkedBigNumberify(
            './index.rsh:82:13:decimal',
            stdlib.UInt_max,
            1,
          );
          const v262 = await txn1.getOutput('api', 'v261', ctc2, v261);

          const v268 = stdlib.add(
            v217,
            stdlib.checkedBigNumberify(
              './index.rsh:87:53:decimal',
              stdlib.UInt_max,
              300,
            ),
          );
          const v269 = stdlib.gt(v268, v211);
          const v270 = stdlib.add(
            v211,
            stdlib.checkedBigNumberify(
              './index.rsh:87:86:decimal',
              stdlib.UInt_max,
              600,
            ),
          );
          const v271 = v269 ? v270 : v211;
          const v884 = v250;
          const v885 = v271;
          const v886 = v245;
          const v889 = v248;
          const v890 = v260;
          const v891 = v222;
          sim_r.isHalt = false;

          break;
        }
        case 'Bid_close0': {
          const v282 = v246[1];

          break;
        }
      }
      return sim_r;
    },
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc0, ctc1, ctc2, ctc2, ctc0, ctc2, ctc2, ctc2, ctc4],
    waitIfNotPresent: false,
  });
  const {
    data: [v246],
    secs: v248,
    time: v247,
    didSend: v133,
    from: v245,
  } = txn1;
  switch (v246[0]) {
    case 'Bid_bid0': {
      const v249 = v246[1];
      const v250 =
        v249[
          stdlib.checkedBigNumberify(
            './index.rsh:76:11:spread',
            stdlib.UInt_max,
            0,
          )
        ];
      const v253 = stdlib.add(v221, v250);
      const v256 = stdlib.gt(v250, v210);
      stdlib.assert(v256, {
        at: './index.rsh:80:18:application',
        fs: [
          'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
        ],
        msg: null,
        who: 'Bid_bid',
      });
      const v260 = stdlib.sub(v253, v210);
      const v261 = stdlib.checkedBigNumberify(
        './index.rsh:82:13:decimal',
        stdlib.UInt_max,
        1,
      );
      const v262 = await txn1.getOutput('api', 'v261', ctc2, v261);
      if (v133) {
        stdlib.protect(ctc5, await interact.out(v249, v262), {
          at: './index.rsh:76:11:application',
          fs: [
            'at ./index.rsh:76:11:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
            'at ./index.rsh:82:12:application call to "k" (defined at: ./index.rsh:76:11:function exp)',
            'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
          ],
          msg: 'out',
          who: 'Bid_bid',
        });
      } else {
      }

      const v268 = stdlib.add(
        v217,
        stdlib.checkedBigNumberify(
          './index.rsh:87:53:decimal',
          stdlib.UInt_max,
          300,
        ),
      );
      const v269 = stdlib.gt(v268, v211);
      const v270 = stdlib.add(
        v211,
        stdlib.checkedBigNumberify(
          './index.rsh:87:86:decimal',
          stdlib.UInt_max,
          600,
        ),
      );
      const v271 = v269 ? v270 : v211;
      const v884 = v250;
      const v885 = v271;
      const v886 = v245;
      const v889 = v248;
      const v890 = v260;
      const v891 = v222;
      return;

      break;
    }
    case 'Bid_close0': {
      const v282 = v246[1];
      return;
      break;
    }
  }
}
export async function Bid_close(ctcTop, interact) {
  if (typeof ctcTop !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(
      new Error(
        `The backend for Bid_close expects to receive a contract as its first argument.`,
      ),
    );
  }
  if (typeof interact !== 'object') {
    return Promise.reject(
      new Error(
        `The backend for Bid_close expects to receive an interact object as its second argument.`,
      ),
    );
  }
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_Address;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_UInt;
  const ctc3 = stdlib.T_Tuple([ctc2]);
  const ctc4 = stdlib.T_Data({
    Bid_bid0: ctc3,
    Bid_close0: ctc3,
  });
  const ctc5 = stdlib.T_Null;

  const [v196, v197, v210, v211, v212, v217, v221, v222] = await ctc.getState(
    stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 4),
    [ctc0, ctc1, ctc2, ctc2, ctc0, ctc2, ctc2, ctc2],
  );
  const v227 = stdlib.protect(ctc3, await interact.in(), {
    at: './index.rsh:1:23:application',
    fs: [
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:63:11:function exp)',
      'at ./index.rsh:53:19:application call to "runBid_close0" (defined at: ./index.rsh:53:19:function exp)',
      'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:53:19:function exp)',
    ],
    msg: 'in',
    who: 'Bid_close',
  });

  const v243 = ['Bid_close0', v227];

  const txn1 = await ctc.sendrecv({
    args: [v196, v197, v210, v211, v212, v217, v221, v222, v243],
    evt_cnt: 1,
    funcNum: 3,
    lct: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc4],
    pay: [
      stdlib.checkedBigNumberify(
        './index.rsh:64:17:decimal',
        stdlib.UInt_max,
        0,
      ),
      [],
    ],
    sim_p: async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };

      const {
        data: [v246],
        secs: v248,
        time: v247,
        didSend: v133,
        from: v245,
      } = txn1;

      switch (v246[0]) {
        case 'Bid_bid0': {
          const v249 = v246[1];

          break;
        }
        case 'Bid_close0': {
          const v282 = v246[1];
          sim_r.txns.push({
            amt: stdlib.checkedBigNumberify(
              './index.rsh:64:17:decimal',
              stdlib.UInt_max,
              0,
            ),
            kind: 'to',
            tok: undefined,
          });
          const v308 = stdlib.checkedBigNumberify(
            './index.rsh:66:13:decimal',
            stdlib.UInt_max,
            1,
          );
          const v309 = await txn1.getOutput('api', 'v308', ctc2, v308);

          sim_r.txns.push({
            amt: v222,
            kind: 'from',
            to: v212,
            tok: v197,
          });
          sim_r.txns.push({
            amt: v221,
            kind: 'from',
            to: v196,
            tok: undefined,
          });
          sim_r.txns.push({
            kind: 'halt',
            tok: v197,
          });
          sim_r.txns.push({
            kind: 'halt',
            tok: undefined,
          });
          sim_r.isHalt = true;

          break;
        }
      }
      return sim_r;
    },
    soloSend: false,
    timeoutAt: undefined,
    tys: [ctc0, ctc1, ctc2, ctc2, ctc0, ctc2, ctc2, ctc2, ctc4],
    waitIfNotPresent: false,
  });
  const {
    data: [v246],
    secs: v248,
    time: v247,
    didSend: v133,
    from: v245,
  } = txn1;
  switch (v246[0]) {
    case 'Bid_bid0': {
      const v249 = v246[1];
      return;
      break;
    }
    case 'Bid_close0': {
      const v282 = v246[1];
      const v308 = stdlib.checkedBigNumberify(
        './index.rsh:66:13:decimal',
        stdlib.UInt_max,
        1,
      );
      const v309 = await txn1.getOutput('api', 'v308', ctc2, v308);
      if (v133) {
        stdlib.protect(ctc5, await interact.out(v282, v309), {
          at: './index.rsh:63:11:application',
          fs: [
            'at ./index.rsh:63:11:application call to [unknown function] (defined at: ./index.rsh:63:11:function exp)',
            'at ./index.rsh:66:12:application call to "k" (defined at: ./index.rsh:63:11:function exp)',
            'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:63:11:function exp)',
          ],
          msg: 'out',
          who: 'Bid_close',
        });
      } else {
      }

      return;

      break;
    }
  }
}
export async function Seller(ctcTop, interact) {
  if (typeof ctcTop !== 'object' || ctcTop._initialize === undefined) {
    return Promise.reject(
      new Error(
        `The backend for Seller expects to receive a contract as its first argument.`,
      ),
    );
  }
  if (typeof interact !== 'object') {
    return Promise.reject(
      new Error(
        `The backend for Seller expects to receive an interact object as its second argument.`,
      ),
    );
  }
  const ctc = ctcTop._initialize();
  const stdlib = ctc.stdlib;
  const ctc0 = stdlib.T_UInt;
  const ctc1 = stdlib.T_Token;
  const ctc2 = stdlib.T_Object({
    dl: ctc0,
    it: ctc1,
  });
  const ctc3 = stdlib.T_Null;
  const ctc4 = stdlib.T_Tuple([ctc0]);
  const ctc5 = stdlib.T_Data({
    Bid_bid0: ctc4,
    Bid_close0: ctc4,
  });
  const ctc6 = stdlib.T_Address;

  const v193 = stdlib.protect(ctc2, await interact.getParams(), {
    at: './index.rsh:36:38:application',
    fs: [
      'at ./index.rsh:32:14:application call to [unknown function] (defined at: ./index.rsh:32:18:function exp)',
    ],
    msg: 'getParams',
    who: 'Seller',
  });
  const v194 = v193.it;
  const v195 = v193.dl;

  const txn1 = await ctc.sendrecv({
    args: [v194, v195],
    evt_cnt: 2,
    funcNum: 0,
    lct: stdlib.checkedBigNumberify('./index.rsh:39:6:dot', stdlib.UInt_max, 0),
    onlyIf: true,
    out_tys: [ctc1, ctc0],
    pay: [
      stdlib.checkedBigNumberify('./index.rsh:decimal', stdlib.UInt_max, 0),
      [],
    ],
    sim_p: async (txn1) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };

      const {
        data: [v197, v198],
        secs: v200,
        time: v199,
        didSend: v27,
        from: v196,
      } = txn1;

      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify('<builtin>', stdlib.UInt_max, 0),
        kind: 'init',
        tok: v197,
      });
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify(
          './index.rsh:decimal',
          stdlib.UInt_max,
          0,
        ),
        kind: 'to',
        tok: undefined,
      });
      sim_r.isHalt = false;

      return sim_r;
    },
    soloSend: true,
    timeoutAt: undefined,
    tys: [ctc1, ctc0],
    waitIfNotPresent: false,
  });
  const {
    data: [v197, v198],
    secs: v200,
    time: v199,
    didSend: v27,
    from: v196,
  } = txn1;
  const txn2 = await ctc.sendrecv({
    args: [v196, v197, v198],
    evt_cnt: 0,
    funcNum: 1,
    lct: v199,
    onlyIf: true,
    out_tys: [],
    pay: [
      stdlib.checkedBigNumberify('./index.rsh:45:6:dot', stdlib.UInt_max, 0),
      [
        [
          stdlib.checkedBigNumberify(
            './index.rsh:45:12:decimal',
            stdlib.UInt_max,
            1,
          ),
          v197,
        ],
      ],
    ],
    sim_p: async (txn2) => {
      const sim_r = { txns: [], mapRefs: [], mapsPrev: [], mapsNext: [] };

      const {
        data: [],
        secs: v204,
        time: v203,
        didSend: v35,
        from: v202,
      } = txn2;

      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify(
          './index.rsh:45:6:dot',
          stdlib.UInt_max,
          0,
        ),
        kind: 'to',
        tok: undefined,
      });
      sim_r.txns.push({
        amt: stdlib.checkedBigNumberify(
          './index.rsh:45:12:decimal',
          stdlib.UInt_max,
          1,
        ),
        kind: 'to',
        tok: v197,
      });
      const v208 = stdlib.addressEq(v196, v202);
      stdlib.assert(v208, {
        at: './index.rsh:45:6:dot',
        fs: [],
        msg: 'sender correct',
        who: 'Seller',
      });

      const v210 = stdlib.checkedBigNumberify(
        './index.rsh:53:35:decimal',
        stdlib.UInt_max,
        0,
      );
      const v211 = v198;
      const v212 = v196;
      const v213 = true;
      const v214 = v203;
      const v217 = v204;
      const v221 = stdlib.checkedBigNumberify(
        './index.rsh:30:11:after expr stmt semicolon',
        stdlib.UInt_max,
        0,
      );
      const v222 = stdlib.checkedBigNumberify(
        './index.rsh:45:12:decimal',
        stdlib.UInt_max,
        1,
      );

      if (
        (() => {
          return v213;
        })()
      ) {
        sim_r.isHalt = false;
      } else {
        sim_r.txns.push({
          amt: v222,
          kind: 'from',
          to: v212,
          tok: v197,
        });
        sim_r.txns.push({
          amt: v221,
          kind: 'from',
          to: v196,
          tok: undefined,
        });
        sim_r.txns.push({
          kind: 'halt',
          tok: v197,
        });
        sim_r.txns.push({
          kind: 'halt',
          tok: undefined,
        });
        sim_r.isHalt = true;
      }
      return sim_r;
    },
    soloSend: true,
    timeoutAt: undefined,
    tys: [ctc6, ctc1, ctc0],
    waitIfNotPresent: false,
  });
  const {
    data: [],
    secs: v204,
    time: v203,
    didSend: v35,
    from: v202,
  } = txn2;
  const v208 = stdlib.addressEq(v196, v202);
  stdlib.assert(v208, {
    at: './index.rsh:45:6:dot',
    fs: [],
    msg: 'sender correct',
    who: 'Seller',
  });
  stdlib.protect(ctc3, await interact.signal(), {
    at: './index.rsh:1:39:application',
    fs: [
      'at ./index.rsh:1:21:application call to [unknown function] (defined at: ./index.rsh:1:25:function exp)',
      'at ./index.rsh:47:25:application call to "liftedInteract" (defined at: ./index.rsh:47:25:application)',
    ],
    msg: 'signal',
    who: 'Seller',
  });

  let v210 = stdlib.checkedBigNumberify(
    './index.rsh:53:35:decimal',
    stdlib.UInt_max,
    0,
  );
  let v211 = v198;
  let v212 = v196;
  let v213 = true;
  let v214 = v203;
  let v217 = v204;
  let v221 = stdlib.checkedBigNumberify(
    './index.rsh:30:11:after expr stmt semicolon',
    stdlib.UInt_max,
    0,
  );
  let v222 = stdlib.checkedBigNumberify(
    './index.rsh:45:12:decimal',
    stdlib.UInt_max,
    1,
  );

  while (
    (() => {
      return v213;
    })()
  ) {
    const txn3 = await ctc.recv({
      didSend: false,
      evt_cnt: 1,
      funcNum: 3,
      out_tys: [ctc5],
      timeoutAt: undefined,
      waitIfNotPresent: false,
    });
    const {
      data: [v246],
      secs: v248,
      time: v247,
      didSend: v133,
      from: v245,
    } = txn3;
    switch (v246[0]) {
      case 'Bid_bid0': {
        const v249 = v246[1];
        const v250 =
          v249[
            stdlib.checkedBigNumberify(
              './index.rsh:76:11:spread',
              stdlib.UInt_max,
              0,
            )
          ];
        const v253 = stdlib.add(v221, v250);
        const v256 = stdlib.gt(v250, v210);
        stdlib.assert(v256, {
          at: './index.rsh:80:18:application',
          fs: [
            'at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)',
          ],
          msg: null,
          who: 'Seller',
        });
        const v260 = stdlib.sub(v253, v210);
        const v261 = stdlib.checkedBigNumberify(
          './index.rsh:82:13:decimal',
          stdlib.UInt_max,
          1,
        );
        await txn3.getOutput('api', 'v261', ctc0, v261);
        const v268 = stdlib.add(
          v217,
          stdlib.checkedBigNumberify(
            './index.rsh:87:53:decimal',
            stdlib.UInt_max,
            300,
          ),
        );
        const v269 = stdlib.gt(v268, v211);
        const v270 = stdlib.add(
          v211,
          stdlib.checkedBigNumberify(
            './index.rsh:87:86:decimal',
            stdlib.UInt_max,
            600,
          ),
        );
        const v271 = v269 ? v270 : v211;
        const cv210 = v250;
        const cv211 = v271;
        const cv212 = v245;
        const cv213 = true;
        const cv214 = v247;
        const cv217 = v248;
        const cv221 = v260;
        const cv222 = v222;

        v210 = cv210;
        v211 = cv211;
        v212 = cv212;
        v213 = cv213;
        v214 = cv214;
        v217 = cv217;
        v221 = cv221;
        v222 = cv222;

        continue;
        break;
      }
      case 'Bid_close0': {
        const v282 = v246[1];
        const v308 = stdlib.checkedBigNumberify(
          './index.rsh:66:13:decimal',
          stdlib.UInt_max,
          1,
        );
        await txn3.getOutput('api', 'v308', ctc0, v308);
        const cv210 = v210;
        const cv211 = v211;
        const cv212 = v212;
        const cv213 = false;
        const cv214 = v247;
        const cv217 = v248;
        const cv221 = v221;
        const cv222 = v222;

        v210 = cv210;
        v211 = cv211;
        v212 = cv212;
        v213 = cv213;
        v214 = cv214;
        v217 = cv217;
        v221 = cv221;
        v222 = cv222;

        continue;
        break;
      }
    }
  }
  return;
}
const _ALGO = {
  appApproval: `#pragma version 5
txn RekeyTo
global ZeroAddress
==
assert
txn Lease
global ZeroAddress
==
assert
int 0
store 0
txn ApplicationID
bz alloc
byte base64()
app_global_get
dup
int 0
extract_uint64
store 1
dup
int 8
extract_uint64
store 2
extract 16 32
store 3
txn NumAppArgs
int 3
==
assert
txna ApplicationArgs 0
btoi
preamble:
// Handler 0
dup
int 0
==
bz l0_afterHandler0
pop
// check step
int 0
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
byte base64()
pop
txna ApplicationArgs 2
dup
len
int 48
==
assert
dup
extract 0 32
store 255
dup
int 32
extract_uint64
store 254
dup
int 40
extract_uint64
store 253
pop
txn Sender
global CreatorAddress
==
assert
load 255
store 3
// "CheckPay"
// "./index.rsh:39:6:dot"
// "[]"
int 100000
dup
bz l1_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Receiver
==
assert
l1_checkTxnK:
pop
// Initializing token
int 100000
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Receiver
==
assert
l2_checkTxnK:
pop
int 0
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns AssetAmount
==
assert
load 254
dig 1
gtxns XferAsset
==
assert
int axfer
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
load 3
dig 1
gtxns AssetReceiver
==
assert
l3_checkTxnK:
pop
// "CheckPay"
// "./index.rsh:39:6:dot"
// "[]"
txn Sender
load 254
itob
concat
load 253
itob
concat
int 1
bzero
dig 1
extract 0 48
app_global_put
pop
int 1
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l0_afterHandler0:
// Handler 1
dup
int 1
==
bz l4_afterHandler1
pop
// check step
int 1
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
extract 0 32
store 255
dup
int 32
extract_uint64
store 254
dup
int 40
extract_uint64
store 253
pop
txna ApplicationArgs 2
dup
len
int 0
==
assert
pop
// "CheckPay"
// "./index.rsh:45:6:dot"
// "[]"
// "CheckPay"
// "./index.rsh:45:6:dot"
// "[]"
int 1
dup
bz l5_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns AssetAmount
==
assert
load 254
dig 1
gtxns XferAsset
==
assert
int axfer
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns AssetReceiver
==
assert
l5_checkTxnK:
pop
// Just "sender correct"
// "./index.rsh:45:6:dot"
// "[]"
load 255
txn Sender
==
assert
load 255
load 254
itob
concat
int 8
bzero
load 253
itob
concat
load 255
concat
int 1
itob // bool
substring 7 8
concat
global Round
itob
concat
global LatestTimestamp
itob
concat
int 8
bzero
concat
byte base64(AAAAAAAAAAE=)
concat
b loopBody2
l4_afterHandler1:
l6_afterHandler2:
// Handler 3
dup
int 3
==
bz l7_afterHandler3
pop
// check step
int 4
load 1
==
assert
// check time
txna ApplicationArgs 1
btoi
dup
int 0
==
swap
load 2
==
||
assert
int 1
bzero
app_global_get
dup
extract 0 32
store 255
dup
int 32
extract_uint64
store 254
dup
int 40
extract_uint64
store 253
dup
int 48
extract_uint64
store 252
dup
extract 56 32
store 251
dup
int 88
extract_uint64
store 250
dup
int 96
extract_uint64
store 249
dup
int 104
extract_uint64
store 248
pop
txna ApplicationArgs 2
dup
len
int 9
==
assert
dup
store 247
pop
load 247
int 0
getbyte
int 0
==
bz l9_switchAfterBid_bid0
load 247
extract 1 8
dup
store 246
btoi
store 245
// "CheckPay"
// "./index.rsh:53:19:dot"
// "[]"
load 245
dup
bz l10_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Receiver
==
assert
l10_checkTxnK:
pop
// Nothing
// "./index.rsh:80:18:application"
// "[at ./index.rsh:53:19:application call to [unknown function] (defined at: ./index.rsh:76:11:function exp)]"
load 245
load 253
>
assert
load 253
dup
bz l11_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
load 251
dig 1
gtxns Receiver
==
assert
l11_checkTxnK:
pop
byte base64(AAAAAAAAAQUAAAAAAAAAAQ==)
log // 16
int 1
load 255
load 254
itob
concat
load 245
itob
load 252
dup
int 600
+
load 250
int 300
+
load 252
>
select
itob
concat
txn Sender
concat
int 1
itob // bool
substring 7 8
concat
global Round
itob
concat
global LatestTimestamp
itob
concat
load 249
load 245
+
load 253
-
itob
concat
load 248
itob
concat
b loopBody2
l9_switchAfterBid_bid0:
load 247
int 0
getbyte
int 1
==
bz l12_switchAfterBid_close0
// "CheckPay"
// "./index.rsh:53:19:dot"
// "[]"
byte base64(AAAAAAAAATQAAAAAAAAAAQ==)
log // 16
int 1
load 255
load 254
itob
concat
load 253
itob
load 252
itob
concat
load 251
concat
int 0
itob // bool
substring 7 8
concat
global Round
itob
concat
global LatestTimestamp
itob
concat
load 249
itob
concat
load 248
itob
concat
b loopBody2
l12_switchAfterBid_close0:
l8_switchK:
l7_afterHandler3:
int 0
assert
loopBody2:
dup
int 0
extract_uint64
store 255
dup
int 8
extract_uint64
store 254
dup
extract 16 32
store 253
dup
extract 48 1
btoi
store 252
dup
int 49
extract_uint64
store 251
dup
int 57
extract_uint64
store 250
dup
int 65
extract_uint64
store 249
dup
int 73
extract_uint64
store 248
pop
dup
extract 0 32
store 247
dup
int 32
extract_uint64
store 246
pop
load 252
bz l13_ifF
load 247
load 246
itob
concat
load 255
itob
concat
load 254
itob
concat
load 253
concat
load 250
itob
concat
load 249
itob
concat
load 248
itob
concat
int 1
bzero
dig 1
extract 0 112
app_global_put
pop
int 4
store 1
global Round
store 2
txn OnCompletion
int NoOp
==
assert
b updateState
l13_ifF:
load 248
dup
bz l14_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns AssetAmount
==
assert
load 246
dig 1
gtxns XferAsset
==
assert
int axfer
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
load 253
dig 1
gtxns AssetReceiver
==
assert
l14_checkTxnK:
pop
load 249
dup
bz l15_checkTxnK
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
load 247
dig 1
gtxns Receiver
==
assert
l15_checkTxnK:
pop
int 0
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns AssetAmount
==
assert
load 246
dig 1
gtxns XferAsset
==
assert
int axfer
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
global CreatorAddress
dig 1
gtxns AssetCloseTo
==
assert
l16_checkTxnK:
pop
int 0
load 0
dup
int 1
+
store 0
swap
dig 1
gtxns Amount
==
assert
int pay
dig 1
gtxns TypeEnum
==
assert
int 0
dig 1
gtxns Fee
==
assert
global ZeroAddress
dig 1
gtxns Lease
==
assert
global ZeroAddress
dig 1
gtxns RekeyTo
==
assert
load 3
dig 1
gtxns Sender
==
assert
global CreatorAddress
dig 1
gtxns CloseRemainderTo
==
assert
l17_checkTxnK:
pop
txn OnCompletion
int DeleteApplication
==
assert
updateState:
byte base64()
load 1
itob
load 2
itob
load 3
concat
concat
app_global_put
checkSize:
load 0
dup
dup
int 1
+
global GroupSize
==
assert
txn GroupIndex
==
assert
int 1000
*
txn Fee
<=
assert
done:
int 1
return
alloc:
txn OnCompletion
int NoOp
==
assert
int 0
store 1
int 0
store 2
global ZeroAddress
store 3
b updateState
`,
  appClear: `#pragma version 5
int 0
`,
  escrow: `#pragma version 5
global GroupSize
int 1
-
dup
gtxns TypeEnum
int appl
==
assert
gtxns ApplicationID
int {{ApplicationID}}
==
assert
done:
int 1
`,
  mapDataKeys: 0,
  mapDataSize: 0,
  stateKeys: 1,
  stateSize: 112,
  unsupported: [],
  version: 5,
};
const _ETH = {
  ABI: `[
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v197",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v198",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "stateMutability": "payable",
    "type": "constructor"
  },
  {
    "inputs": [
      {
        "internalType": "uint256",
        "name": "msg",
        "type": "uint256"
      }
    ],
    "name": "ReachError",
    "type": "error"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "internalType": "address payable",
                "name": "v197",
                "type": "address"
              },
              {
                "internalType": "uint256",
                "name": "v198",
                "type": "uint256"
              }
            ],
            "internalType": "struct T2",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T3",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e0",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "indexed": false,
        "internalType": "struct T8",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e1",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T10",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "internalType": "uint256",
                        "name": "elem0",
                        "type": "uint256"
                      }
                    ],
                    "internalType": "struct T9",
                    "name": "_Bid_bid0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "internalType": "uint256",
                        "name": "elem0",
                        "type": "uint256"
                      }
                    ],
                    "internalType": "struct T9",
                    "name": "_Bid_close0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T10",
                "name": "v246",
                "type": "tuple"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "indexed": false,
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "e3",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "v261",
        "type": "uint256"
      }
    ],
    "name": "oe_v261",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "v308",
        "type": "uint256"
      }
    ],
    "name": "oe_v308",
    "type": "event"
  },
  {
    "stateMutability": "payable",
    "type": "fallback"
  },
  {
    "inputs": [],
    "name": "Auction_currentPrice",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "Auction_endSecs",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "Auction_highestBidder",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "Auction_nft",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentState",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      },
      {
        "internalType": "bytes",
        "name": "",
        "type": "bytes"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "_reachCurrentTime",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "internalType": "bool",
            "name": "msg",
            "type": "bool"
          }
        ],
        "internalType": "struct T8",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m1",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "components": [
          {
            "internalType": "uint256",
            "name": "time",
            "type": "uint256"
          },
          {
            "components": [
              {
                "components": [
                  {
                    "internalType": "enum _enum_T10",
                    "name": "which",
                    "type": "uint8"
                  },
                  {
                    "components": [
                      {
                        "internalType": "uint256",
                        "name": "elem0",
                        "type": "uint256"
                      }
                    ],
                    "internalType": "struct T9",
                    "name": "_Bid_bid0",
                    "type": "tuple"
                  },
                  {
                    "components": [
                      {
                        "internalType": "uint256",
                        "name": "elem0",
                        "type": "uint256"
                      }
                    ],
                    "internalType": "struct T9",
                    "name": "_Bid_close0",
                    "type": "tuple"
                  }
                ],
                "internalType": "struct T10",
                "name": "v246",
                "type": "tuple"
              }
            ],
            "internalType": "struct T11",
            "name": "msg",
            "type": "tuple"
          }
        ],
        "internalType": "struct T12",
        "name": "_a",
        "type": "tuple"
      }
    ],
    "name": "m3",
    "outputs": [],
    "stateMutability": "payable",
    "type": "function"
  },
  {
    "stateMutability": "payable",
    "type": "receive"
  }
]`,
  Bytecode: `0x6080604052604051620019c1380380620019c183398101604081905262000026916200021b565b60008055604080518251815260208084015180516001600160a01b03168284015201518183015290517f7ecf76c8d208be798ea0a2a970c5e54deca709f3d76a10dae3b1cf06299cc7f99181900360600190a1620000873415600b62000114565b604080516060808201835260006020808401828152848601838152338087528884018051516001600160a01b0390811685529051850151835260019586905543909555875193840152905190921694810194909452519083015290608001604051602081830303815290604052600290805190602001906200010b9291906200013e565b505050620002d0565b816200013a5760405163100960cb60e01b81526004810182905260240160405180910390fd5b5050565b8280546200014c9062000293565b90600052602060002090601f016020900481019282620001705760008555620001bb565b82601f106200018b57805160ff1916838001178555620001bb565b82800160010185558215620001bb579182015b82811115620001bb5782518255916020019190600101906200019e565b50620001c9929150620001cd565b5090565b5b80821115620001c95760008155600101620001ce565b604080519081016001600160401b03811182821017156200021557634e487b7160e01b600052604160045260246000fd5b60405290565b600081830360608112156200022f57600080fd5b62000239620001e4565b835181526040601f19830112156200025057600080fd5b6200025a620001e4565b60208501519092506001600160a01b03811681146200027857600080fd5b82526040939093015160208083019190915283015250919050565b600181811c90821680620002a857607f821691505b60208210811415620002ca57634e487b7160e01b600052602260045260246000fd5b50919050565b6116e180620002e06000396000f3fe6080604052600436106100795760003560e01c8063832307571161004b57806383230757146100ff5780639d4c01ec14610114578063ab53f2c614610127578063d336c0691461014a57005b80631fa4d584146100825780633ccbd570146100b457806342e36575146100d75780637963168e146100ec57005b3661008057005b005b34801561008e57600080fd5b5061009761015f565b6040516001600160a01b0390911681526020015b60405180910390f35b3480156100c057600080fd5b506100c96102e0565b6040519081526020016100ab565b3480156100e357600080fd5b5061009761045e565b6100806100fa36600461130d565b6105dc565b34801561010b57600080fd5b506001546100c9565b610080610122366004611325565b6107ab565b34801561013357600080fd5b5061013c610b7d565b6040516100ab929190611363565b34801561015657600080fd5b506100c9610c1a565b60006001600054141561021c5760006002805461017b9061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546101a79061139d565b80156101f45780601f106101c9576101008083540402835291602001916101f4565b820191906000526020600020905b8154815290600101906020018083116101d757829003601f168201915b505050505080602001905181019061020c91906113ee565b905061021a60006009610d94565b505b600460005414156102d1576000600280546102369061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546102629061139d565b80156102af5780601f10610284576101008083540402835291602001916102af565b820191906000526020600020905b81548152906001019060200180831161029257829003601f168201915b50505050508060200190518101906102c79190611464565b6080015192915050565b6102dd60006009610d94565b90565b60006001600054141561039d576000600280546102fc9061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546103289061139d565b80156103755780601f1061034a57610100808354040283529160200191610375565b820191906000526020600020905b81548152906001019060200180831161035857829003601f168201915b505050505080602001905181019061038d91906113ee565b905061039b60006008610d94565b505b60046000541415610452576000600280546103b79061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546103e39061139d565b80156104305780601f1061040557610100808354040283529160200191610430565b820191906000526020600020905b81548152906001019060200180831161041357829003601f168201915b50505050508060200190518101906104489190611464565b6060015192915050565b6102dd60006008610d94565b60006001600054141561051b5760006002805461047a9061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546104a69061139d565b80156104f35780601f106104c8576101008083540402835291602001916104f3565b820191906000526020600020905b8154815290600101906020018083116104d657829003601f168201915b505050505080602001905181019061050b91906113ee565b90506105196000600a610d94565b505b600460005414156105d0576000600280546105359061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546105619061139d565b80156105ae5780601f10610583576101008083540402835291602001916105ae565b820191906000526020600020905b81548152906001019060200180831161059157829003601f168201915b50505050508060200190518101906105c69190611464565b6020015192915050565b6102dd6000600a610d94565b6105ec600160005414600f610d94565b610606813515806105ff57506001548235145b6010610d94565b6000808055600280546106189061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546106449061139d565b80156106915780601f1061066657610100808354040283529160200191610691565b820191906000526020600020905b81548152906001019060200180831161067457829003601f168201915b50505050508060200190518101906106a991906113ee565b90507f9f41c6cf17ede288cbb2cfbbafdd05b2b2025dea3b047cdb79dbc892d7a9286d826040516106da9190611525565b60405180910390a16106ee3415600c610d94565b6107086107013383602001516001610dbe565b600d610d94565b8051610720906001600160a01b03163314600e610d94565b6107286111c4565b815181516001600160a01b0391821690526020808401518351908316908201528083018051600090819052604080870151835190940193909352855182519416939092019290925281516001606090910181905282514360809091015282514260a090910152825160c00191909152905160e001526107a681610dd6565b505050565b6107bb6004600054146014610d94565b6107d5813515806107ce57506001548235145b6015610d94565b6000808055600280546107e79061139d565b80601f01602080910402602001604051908101604052809291908181526020018280546108139061139d565b80156108605780601f1061083557610100808354040283529160200191610860565b820191906000526020600020905b81548152906001019060200180831161084357829003601f168201915b50505050508060200190518101906108789190611464565b905061089560408051808201909152600060208201908152815290565b7fc365789f23f0e328b18daf69c6cf0345c0a9df1b0f961618474c23d8c93461a0836040516108c4919061156f565b60405180910390a160006108de60408501602086016115c2565b60018111156108ef576108ef611559565b1415610a8457610907368490038401604085016115dd565b808252516109189034146011610d94565b604082015181515161092c91106012610d94565b81608001516001600160a01b03166108fc83604001519081150290604051600060405180830381858888f1935050505015801561096d573d6000803e3d6000fd5b50604051600181527f050cc91016aef72708153e0ca1561a0f62be43b4768e3f3882f4cd53f8f055239060200160405180910390a16109aa6111c4565b825181516001600160a01b03918216905260208085015183519216918101919091528251519082015152606083015160a08401516109eb9061012c90611643565b116109fa578260600151610a0c565b6102588360600151610a0c9190611643565b60208083018051909101919091528051336040918201528151600160609091015281514360809091015290514260a09091015283015182515160c0850151610a549190611643565b610a5e919061165b565b60208201805160c0019190915260e08085015191510152610a7e81610dd6565b50505050565b6001610a9660408501602086016115c2565b6001811115610aa757610aa7611559565b14156107a657610ab934156013610d94565b604051600181527fa1eac5ce9bd9e9a0da1a262912af7b924e2d80f1ef9e13d7f2b50e72530835679060200160405180910390a1610af56111c4565b825181516001600160a01b0391821690526020808501518351908316908201526040808601518285018051919091526060808801518251909401939093526080808801518251951694909201939093528251600092019190915281514391015280514260a09091015260c08085015182519091015260e08085015191510152610a7e81610dd6565b600060606000546002808054610b929061139d565b80601f0160208091040260200160405190810160405280929190818152602001828054610bbe9061139d565b8015610c0b5780601f10610be057610100808354040283529160200191610c0b565b820191906000526020600020905b815481529060010190602001808311610bee57829003601f168201915b50505050509050915091509091565b600060016000541415610cd757600060028054610c369061139d565b80601f0160208091040260200160405190810160405280929190818152602001828054610c629061139d565b8015610caf5780601f10610c8457610100808354040283529160200191610caf565b820191906000526020600020905b815481529060010190602001808311610c9257829003601f168201915b5050505050806020019051810190610cc791906113ee565b9050610cd560006007610d94565b505b60046000541415610d8c57600060028054610cf19061139d565b80601f0160208091040260200160405190810160405280929190818152602001828054610d1d9061139d565b8015610d6a5780601f10610d3f57610100808354040283529160200191610d6a565b820191906000526020600020905b815481529060010190602001808311610d4d57829003601f168201915b5050505050806020019051810190610d829190611464565b6040015192915050565b6102dd600060075b81610dba5760405163100960cb60e01b8152600481018290526024015b60405180910390fd5b5050565b6000610dcc83853085610fca565b90505b9392505050565b80602001516060015115610f4b57610e4760405180610100016040528060006001600160a01b0316815260200160006001600160a01b03168152602001600081526020016000815260200160006001600160a01b031681526020016000815260200160008152602001600081525090565b8151516001600160a01b03908116825282516020908101518216818401528084018051516040808601919091528151830151606086015281518101519093166080850152805160a09081015190850152805160c090810151908501525160e090810151908401526004600055436001559051610f279183910160006101008201905060018060a01b0380845116835280602085015116602084015260408401516040840152606084015160608401528060808501511660808401525060a083015160a083015260c083015160c083015260e083015160e083015292915050565b604051602081830303815290604052600290805190602001906107a692919061123e565b610f6e816000015160200151826020015160400151836020015160e001516110a4565b805151602082015160c001516040516001600160a01b039092169181156108fc0291906000818181858888f19350505050158015610fb0573d6000803e3d6000fd5b5060008080556001819055610fc7906002906112c2565b50565b604080516001600160a01b0385811660248301528481166044830152606480830185905283518084039091018152608490920183526020820180516001600160e01b03166323b872dd60e01b17905291516000928392839291891691839161103191611672565b60006040518083038185875af1925050503d806000811461106e576040519150601f19603f3d011682016040523d82523d6000602084013e611073565b606091505b5091509150611084828260016110b8565b5080806020019051810190611099919061168e565b979650505050505050565b6110af8383836110f3565b6107a657600080fd5b606083156110c7575081610dcf565b8251156110d75782518084602001fd5b60405163100960cb60e01b815260048101839052602401610db1565b604080516001600160a01b038481166024830152604480830185905283518084039091018152606490920183526020820180516001600160e01b031663a9059cbb60e01b17905291516000928392839291881691839161115291611672565b60006040518083038185875af1925050503d806000811461118f576040519150601f19603f3d011682016040523d82523d6000602084013e611194565b606091505b50915091506111a5828260026110b8565b50808060200190518101906111ba919061168e565b9695505050505050565b60408051608081018252600091810182815260608201929092529081908152602001611239604051806101000160405280600081526020016000815260200160006001600160a01b03168152602001600015158152602001600081526020016000815260200160008152602001600081525090565b905290565b82805461124a9061139d565b90600052602060002090601f01602090048101928261126c57600085556112b2565b82601f1061128557805160ff19168380011785556112b2565b828001600101855582156112b2579182015b828111156112b2578251825591602001919060010190611297565b506112be9291506112f8565b5090565b5080546112ce9061139d565b6000825580601f106112de575050565b601f016020900490600052602060002090810190610fc791905b5b808211156112be57600081556001016112f9565b60006040828403121561131f57600080fd5b50919050565b60006080828403121561131f57600080fd5b60005b8381101561135257818101518382015260200161133a565b83811115610a7e5750506000910152565b8281526040602082015260008251806040840152611388816060850160208701611337565b601f01601f1916919091016060019392505050565b600181811c908216806113b157607f821691505b6020821081141561131f57634e487b7160e01b600052602260045260246000fd5b80516001600160a01b03811681146113e957600080fd5b919050565b60006060828403121561140057600080fd5b6040516060810181811067ffffffffffffffff8211171561143157634e487b7160e01b600052604160045260246000fd5b60405261143d836113d2565b815261144b602084016113d2565b6020820152604083015160408201528091505092915050565b600061010080838503121561147857600080fd5b6040519081019067ffffffffffffffff821181831017156114a957634e487b7160e01b600052604160045260246000fd5b816040526114b6846113d2565b81526114c4602085016113d2565b602082015260408401516040820152606084015160608201526114e9608085016113d2565b608082015260a084015160a082015260c084015160c082015260e084015160e0820152809250505092915050565b8015158114610fc757600080fd5b8135815260408101602083013561153b81611517565b80151560208401525092915050565b8035600281106113e957600080fd5b634e487b7160e01b600052602160045260246000fd5b81358152608081016115836020840161154a565b600281106115a157634e487b7160e01b600052602160045260246000fd5b80602084015250604083013560408301526060830135606083015292915050565b6000602082840312156115d457600080fd5b610dcf8261154a565b6000602082840312156115ef57600080fd5b6040516020810181811067ffffffffffffffff8211171561162057634e487b7160e01b600052604160045260246000fd5b6040529135825250919050565b634e487b7160e01b600052601160045260246000fd5b600082198211156116565761165661162d565b500190565b60008282101561166d5761166d61162d565b500390565b60008251611684818460208701611337565b9190910192915050565b6000602082840312156116a057600080fd5b8151610dcf8161151756fea26469706673582212209e0d70f79e9b28b73cdefed817d37a22ea38a24bcc6a3f66891cc6d1ffda58c064736f6c63430008090033`,
  BytecodeLen: 6593,
  Which: `oD`,
  version: 4,
  views: {
    Auction: {
      currentPrice: `Auction_currentPrice`,
      endSecs: `Auction_endSecs`,
      highestBidder: `Auction_highestBidder`,
      nft: `Auction_nft`,
    },
  },
};
export const _Connectors = {
  ALGO: _ALGO,
  ETH: _ETH,
};
export const _Participants = {
  Bid_bid: Bid_bid,
  Bid_close: Bid_close,
  Seller: Seller,
};
export const _APIs = {
  Bid: {
    bid: Bid_bid,
    close: Bid_close,
  },
};
