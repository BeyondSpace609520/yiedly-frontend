export const CHAIN_ID = 80001;
export const CHAIN_ID_ERR_MSG = 'Please change your network to Mumbai';
export const METAMASK_INSTALL_ERR_MSG = 'Please check you installed Metamask';

export enum CRYPTO_TYPE {
  ETH = "ETH",
  ALGO = "ALGO"
}

export const NFT_ADDRESS = "0xFCD740e5F476e170D2EeAB08Cf3ae4A7109d89DC";
export const NFT_MARKET_ADDRESS = "0xf2eE7A273a758862B62D059aFe5a97EbC77B3215";
export const NFTStorageKey =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJkaWQ6ZXRocjoweDAxNjkyRWQ4MGJEOGNCMDIyZTgyQkNGYUFhQzZEMjJkN0IzMDM1MzciLCJpc3MiOiJuZnQtc3RvcmFnZSIsImlhdCI6MTYzNTg0NTQzMzg4NSwibmFtZSI6InlpZWxkbHkifQ.3WY5m398T1MSJN7weMmm2aSWvOze6FhmNxcZZckHEUA";

export const infuraProvider = "https://polygon-mumbai.infura.io/v3/d8065d7ef4af41aca71f6a74a36d7be3";
